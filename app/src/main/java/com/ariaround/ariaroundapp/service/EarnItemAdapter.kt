package com.ariaround.ariaroundapp.service

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.browser.customtabs.CustomTabsClient.getPackageName
import androidx.compose.ui.graphics.Color
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ariaround.ariaroundapp.QREarnItem
import com.ariaround.ariaroundapp.R
import org.json.JSONArray
import org.json.JSONObject


class EarnItemAdapter(private val mItems: JSONArray, private val cellClickListener: QREarnItem) : RecyclerView.Adapter<EarnItemAdapter.ViewHolder>() {

    private var lan = "en"
    private val TAG = "EarnItemAdapter"

    private lateinit var context : Context

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txItemName : TextView
        val txItemDescription : TextView
        val imageEarnItem : ImageView
        val btNaxt : Button

        init {

             txItemName = itemView.findViewById<TextView>(R.id.tx_item_name)
             txItemDescription = itemView.findViewById<TextView>(R.id.tx_item_description)
             imageEarnItem = itemView.findViewById<ImageView>(R.id.image_earn_item)
             btNaxt = itemView.findViewById<Button>(R.id.bt_next)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.qr_earn_item_layout, viewGroup, false)

        context = viewGroup.context

        return ViewHolder(view)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mItems.length()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val item: JSONObject = mItems.getJSONObject(position)

        Log.i(TAG , "item $item")

        holder.txItemName.text = item.getString("name_en")
        holder.txItemDescription.text = item.getString("description_en")

        when (item.getString("category")){
            "bottle" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_bottle)
            "can" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_can)
            "book" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_book)
            "quest" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_quest)
            "skill" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_skill)
            "stuff" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_stuff)
            "covid-relief" -> holder.imageEarnItem.setImageResource(R.drawable.ic_earn_covid_relief)
        }

        if (!item.getBoolean("available")){
            holder.btNaxt.background.setTint(
                ContextCompat.getColor(
                    context,
                    R.color.gray
                )
            )
            holder.btNaxt.isClickable = false

            holder.txItemName.setTextColor(ContextCompat.getColor(
                context,
                R.color.gray
            ))
        }

        if (item.getString("category") == "covid-relief"){
            holder.btNaxt.background.setTint(
                ContextCompat.getColor(
                    context,
                    R.color.covid_pink
                )
            )
        }

        holder.btNaxt.setOnClickListener {
            cellClickListener.onEarnCellClickListener(item)
        }

    }

}
