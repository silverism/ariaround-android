package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.UserObject

class RegisterRelation : Fragment() {

    private val commonService: CommonService = CommonService()

    private val userModel: UserObject by activityViewModels()

    private var isSelectRelation = false

    companion object {
        private const val TAG = "RegisterRelation"

        private lateinit var btResident: ImageButton
        private lateinit var btVisitor: ImageButton
        private lateinit var btWorker: ImageButton

        private lateinit var txResident: TextView
        private lateinit var txVisitor: TextView
        private lateinit var txWorker: TextView

        private lateinit var btNext: Button
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.register_relation, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.RELATIONSHIP_QUESTION), lan)
        val txTitle = view?.findViewById<TextView>(R.id.tx_register_relation_title)
        txTitle?.text = title

        val resident = commonService.getDialogWithLanguage(resources.getStringArray(R.array.RELATIONSHIP_AWSNER_RESIDENT), lan)
        txResident = view.findViewById<TextView>(R.id.tx_register_relation_resident)
        txResident.text = resident

        val visitor = commonService.getDialogWithLanguage(resources.getStringArray(R.array.RELATIONSHIP_AWSNER_VISITOR), lan)
        txVisitor = view.findViewById<TextView>(R.id.tx_register_relation_visitor)
        txVisitor.text = visitor

        val worker = commonService.getDialogWithLanguage(resources.getStringArray(R.array.RELATIONSHIP_AWSNER_OFFICE), lan)
        txWorker = view.findViewById<TextView>(R.id.tx_register_relation_worker)
        txWorker.text = worker

        val btBack = view.findViewById<ImageButton>(R.id.bt_relation_back)
        btBack.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        btNext = view.findViewById(R.id.bt_next)
        btNext.setOnClickListener {
            if (isSelectRelation) {
                view.findNavController().navigate(R.id.action_registerRelation_to_registerActivity)
            }
        }

        btResident = view.findViewById<ImageButton>(R.id.bt_register_relation_resident)
        btResident?.setOnClickListener {
            setRelation("RESIDENT")
        }

        btVisitor = view.findViewById<ImageButton>(R.id.bt_register_relation_visitor)
        btVisitor?.setOnClickListener {
            setRelation("VISITOR")
        }

        btWorker = view.findViewById<ImageButton>(R.id.bt_register_relation_worker)
        btWorker?.setOnClickListener {
            setRelation("WORKER")
        }

        return view
    }

    private fun setRelation(relation: String) {

        clearRelation()

        userModel.relation = relation
        isSelectRelation = true

        btNext.setBackgroundColor(resources.getColor(R.color.primary))

        when (relation) {
            "RESIDENT" -> {
                btResident.background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_relation_resident_select);
                txResident.setTextColor(resources.getColor(R.color.primary))
            }
            "VISITOR" -> {
                btVisitor.background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_relation_visitor_select);
                txVisitor.setTextColor(resources.getColor(R.color.primary))
            }
            "WORKER" -> {
                btWorker.background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_relation_worker_select);
                txWorker.setTextColor(resources.getColor(R.color.primary))
            }
        }

        Log.i(TAG, userModel.relation)
    }

    private fun clearRelation(){
        btResident.background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_relation_resident);
        txResident.setTextColor(resources.getColor(R.color.black))
        btVisitor.background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_relation_visitor);
        txVisitor.setTextColor(resources.getColor(R.color.black))
        btWorker.background = ContextCompat.getDrawable(requireActivity(), R.drawable.ic_relation_worker);
        txWorker.setTextColor(resources.getColor(R.color.black))

        userModel.relation = ""
        isSelectRelation = false
    }
}