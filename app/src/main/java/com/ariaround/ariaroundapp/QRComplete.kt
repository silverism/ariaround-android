package com.ariaround.ariaroundapp

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.QRObject
import com.ariaround.ariaroundapp.service.UserObject
import org.json.JSONObject
import java.lang.Integer.parseInt

class QRComplete : Fragment() {

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val qrModel : QRObject by activityViewModels()

    private lateinit var txTitle : TextView
    private lateinit var txCardTitle : TextView
    private lateinit var txCardDetail : TextView
    private lateinit var txItemValue  : TextView
    private lateinit var txItemName  : TextView
    private lateinit var txCoinValue  : TextView
    private lateinit var txCoinUnit  : TextView
    private lateinit var txShopName  : TextView

    companion object {
        private const val TAG = "QRComplete"
        private var lan = "en"
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.qr_complete, container, false)

        val itemLayout : LinearLayout = view?.findViewById<LinearLayout>(R.id.item_layout)!!
        val imageToIcon : ImageView = view?.findViewById<ImageView>(R.id.image_to_icon)!!

        var imageItem : ImageView = view?.findViewById<ImageView>(R.id.image_item)

        txTitle =  view.findViewById(R.id.tx_title)

        txCardTitle = view.findViewById(R.id.tx_card_title)
        txCardDetail = view.findViewById(R.id.tx_card_detail)
        txShopName = view?.findViewById(R.id.tx_shop_name)!!
        txCoinValue = view?.findViewById(R.id.tx_coin_value)!!
        txShopName = view?.findViewById(R.id.tx_shop_name)!!

        Log.i(TAG , "qrData : ${qrModel.qrData}")

        if (qrModel.qrType == "spend"){

            itemLayout.visibility = LinearLayout.GONE
            imageToIcon.visibility = LinearLayout.GONE

            txTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.QR_SCAN_FOR_SPEND), lan)

            txCardTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SPEND_COMPLETE_TITLE), lan)
            txCardDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SPEND_COMPLETE_SUB_TITLE), lan)

            txCoinValue.text = qrModel.qrData.getString("value")
            txShopName.text = qrModel.qrData.getJSONObject("destination").getString("name")


        }else {

            txCardTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EARN_COMPLETE_TITLE), lan)
            txCardDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EARN_COMPLETE_SUB_TITLE), lan)

            when (qrModel.destinationItem.getString("category")) {
                "bottle" -> imageItem.setImageResource(R.drawable.ic_earn_bottle)
                "can" -> imageItem.setImageResource(R.drawable.ic_earn_can)
                "Book" -> imageItem.setImageResource(R.drawable.ic_earn_book)
                "quest" -> imageItem.setImageResource(R.drawable.ic_earn_quest)
                "skill" -> imageItem.setImageResource(R.drawable.ic_earn_skill)
                "stuff" -> imageItem.setImageResource(R.drawable.ic_earn_stuff)
                "covid-relief" -> imageItem.setImageResource(R.drawable.ic_earn_covid_relief)
            }

            val value = parseInt(qrModel.qrData.getString("value"))

            if (value > 0) {
                txCoinValue.text = qrModel.qrData.getString("value")
                txCoinUnit = view.findViewById(R.id.tx_coin_unit)
                txCoinUnit.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ARICOIN_UNIT), lan)
                txShopName.text = qrModel.qrData.getJSONObject("destination").getString("name")
            }else{
                txCoinValue.text = ""
                txCoinUnit = view.findViewById(R.id.tx_coin_unit)
                txCoinUnit.text = ""
                txShopName.text = ""
            }



        }

        val btNext = view.findViewById<Button>(R.id.bt_next)
        btNext.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_NEXT), lan)
        btNext.setOnClickListener {
            view.findNavController().popBackStack(R.id.main , false)
        }

        return view
    }



}