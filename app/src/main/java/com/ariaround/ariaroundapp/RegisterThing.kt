package com.ariaround.ariaroundapp

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.functions.FirebaseFunctionsException
import org.json.JSONObject

class RegisterThing : Fragment() {

    private var commonService: CommonService = CommonService()

    private var thingArray = arrayOf("SKILL", "DISCOUNT", "PARK", "PARKING", "NEIGHBOR", "ENVIRONMENT")
    private val selectThing: MutableList<String> = arrayListOf()

    private val userModel: UserObject by activityViewModels()

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    companion object {
        private const val TAG = "RegisterThing"

        private lateinit var btNext: Button

        private lateinit var btThing1: Button
        private lateinit var btThing2: Button
        private lateinit var btThing3: Button
        private lateinit var btThing4: Button
        private lateinit var btThing5: Button
        private lateinit var btThing6: Button

        private lateinit var loadingFrame : FrameLayout
        private lateinit var loadingImage : ImageView
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.register_thing, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_QUESTION), lan)
        val txTitle = view?.findViewById<TextView>(R.id.tx_register_thing_title)
        txTitle?.text = title

        val thing1 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_AWSNER_SKILL), lan)
        btThing1 = view.findViewById<Button>(R.id.bt_register_thing_1)
        btThing1?.text = thing1

        val thing2 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_AWSNER_DISCOUNT), lan)
        btThing2 = view.findViewById<Button>(R.id.bt_register_thing_2)
        btThing2?.text = thing2

        val thing3 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_AWSNER_PARK), lan)
        btThing3 = view.findViewById<Button>(R.id.bt_register_thing_3)
        btThing3?.text = thing3

        val thing4 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_AWSNER_PARKING), lan)
        btThing4 = view.findViewById<Button>(R.id.bt_register_thing_4)
        btThing4?.text = thing4

        val thing5 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_AWSNER_NEIGHBOR), lan)
        btThing5 = view.findViewById<Button>(R.id.bt_register_thing_5)
        btThing5?.text = thing5

        val thing6 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.THING_AWSNER_ENVIRONMENT), lan)
        btThing6 = view.findViewById<Button>(R.id.bt_register_thing_6)
        btThing6?.text = thing6

        val btBack: ImageButton = view.findViewById(R.id.bt_back)
        btBack.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        // Set action listener

        btThing1?.setOnClickListener {
            setThing(0, btThing1)
        }

        btThing2?.setOnClickListener {
            setThing(1, btThing2)
        }

        btThing3?.setOnClickListener {
            setThing(2, btThing3)
        }

        btThing4?.setOnClickListener {
            setThing(3, btThing4)
        }

        btThing5?.setOnClickListener {
            setThing(4, btThing5)
        }

        btThing6?.setOnClickListener {
            setThing(5, btThing6)
        }

        btNext = view.findViewById(R.id.bt_next)
        btNext.setOnClickListener {
            if (this.selectThing.count() == 3) {
                userModel.thing = this.selectThing.toTypedArray()
                Log.i(TAG, userModel.toString())
                register()
            }
        }

        loadingFrame = view.findViewById<FrameLayout>(R.id.loading_frame)
        loadingFrame.visibility = View.INVISIBLE

        loadingImage = view.findViewById<ImageView>(R.id.loading_image)
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        return view
    }

    private fun setThing(thing: Int, button: Button) {
        val index = this.selectThing.indexOf(thingArray[thing])
        if (index >= 0) {
            this.selectThing.removeAt(index)
            button.setBackgroundColor(resources.getColor(R.color.light_gray))
        } else {
            this.selectThing.add(thingArray[thing])
            button.setBackgroundColor(resources.getColor(R.color.primary))
        }

        if (this.selectThing.count() == 3) {
            btNext.setBackgroundColor(resources.getColor(R.color.primary))
        } else {
            btNext.setBackgroundColor(resources.getColor(R.color.gray))
        }
    }

    private fun register(){

        loadingFrame.visibility = View.VISIBLE

        val data = userModel.generateRegisterJSON()

        Log.i(TAG , "data : $data")

        firebaseFunctionService.createUser(data)
            .addOnCompleteListener(OnCompleteListener { task ->

            loadingFrame.visibility = View.INVISIBLE

            if (!task.isSuccessful) {
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG, "$code : $details")
                }
            }else{

                val result = task.result

                if (result?.get("status") == "Complete") {
                    var rewardData = JSONObject()
                    rewardData.put("uid" , userModel.uid)
                    rewardData.put("item" , "New register")

                    loadingFrame.visibility = View.VISIBLE

                    firebaseFunctionService.reward(rewardData).addOnCompleteListener(OnCompleteListener { task ->

                        if (!task.isSuccessful) {
                            val e = task.exception
                            if (e is FirebaseFunctionsException) {
                                val code = e.code
                                val details = e.details

                                Log.e(TAG, "$code : $details")
                            }
                        }else{

                            if (userModel.referral != "") {
                                var referralData = JSONObject()

                                referralData.put("referralCode" , userModel.referral)

                                firebaseFunctionService.checkReferral(referralData).addOnCompleteListener(OnCompleteListener { task ->

                                    if (!task.isSuccessful) {
                                        val e = task.exception
                                        if (e is FirebaseFunctionsException) {
                                            val code = e.code
                                            val details = e.details

                                            Log.e(TAG, "$code : $details")
                                        }
                                    }else{
                                        view?.findNavController()!!.navigate(R.id.action_registerThing_to_registerComplete)
                                    }
                                })
                            }else{
                                view?.findNavController()!!.navigate(R.id.action_registerThing_to_registerComplete)
                            }


                        }



                    })

                }
            }
        })
            .addOnFailureListener { task ->
                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }

    }

}