package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.UserObject

class RegisterName : Fragment() {

    private val commonService: CommonService = CommonService()

    private val userModel: UserObject by activityViewModels()

    lateinit var editUsername : EditText
    lateinit var editReferral : EditText
    lateinit var btNext : Button

    private var isEnterUsernameComplete = false

    companion object {
        private const val TAG = "LoginOtp"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.register_name, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val txWelcome = view?.findViewById<TextView>(R.id.tx_register_name_welcome)!!
        txWelcome.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.WELCOME), lan )

        val nameTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_NAME), lan )
        val txNameTitle = view?.findViewById<TextView>(R.id.tx_register_name_title)
        txNameTitle?.text = nameTitle

        val nameDesc = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_NAME_DETAIL), lan )
        val txNameDesc = view?.findViewById<TextView>(R.id.tx_register_name_description)
        txNameDesc?.text = nameDesc

        val referralTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_REFERRAL), lan )
        val txReferralTitle = view?.findViewById<TextView>(R.id.tx_register_referral_title)
        txReferralTitle?.text = referralTitle

        val referralDesc = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_REFERRAL_DETAIL), lan )
        val txReferralDesc = view?.findViewById<TextView>(R.id.tx_register_referral_description)
        txReferralDesc?.text = referralDesc

        editUsername = view.findViewById<EditText>(R.id.edittext_username)
        editUsername.addTextChangedListener(textWatcher)

        editReferral = view.findViewById<EditText>(R.id.edittext_referral)
        editReferral.hint = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_REFERRAL), lan )

        val btNextLabel = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_NEXT) , lan)
        btNext = view.findViewById<Button>(R.id.bt_register_name_next)
        btNext?.text = btNextLabel
        btNext?.setOnClickListener {
            println("Register name next")
            if(isEnterUsernameComplete) {
                this.userModel.username = editUsername.text.toString()
                this.userModel.referral = editReferral.text.toString()
                view.findNavController().navigate(R.id.action_registerName_to_registerRelation)
            }
        }



        return view
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s != null && s.isNotEmpty()) {
                btNext.setBackgroundColor(resources.getColor(R.color.primary))
                isEnterUsernameComplete = true
            }else{
                btNext.setBackgroundColor(resources.getColor(R.color.gray))
                isEnterUsernameComplete = false
            }
        }
    }
}