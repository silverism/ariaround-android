package com.ariaround.ariaroundapp

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import com.ariaround.ariaroundapp.service.*
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import com.google.android.gms.tasks.OnCompleteListener
import java.util.*
import androidx.core.content.ContextCompat
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso


class EventTicketList : Fragment() {

    companion object {
        private const val TAG = "EventTicketList"
        private var lan = "en"
    }

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    private var ticketHistoryList : JSONArray = JSONArray()
    private var ticketComingyList : JSONArray = JSONArray()
    private var ticketDisplayList : JSONArray = JSONArray()

    private lateinit var ticketLayout : LinearLayout
    private lateinit var rvContacts : RecyclerView

    private lateinit var  btTicketComing : Button
    private lateinit var  btTicketHistory : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.event_ticket_list, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        btTicketComing = view?.findViewById<Button>(R.id.bt_ticket_coming)!!
        btTicketComing.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_TICKET_LIST_COMING), lan)
        btTicketComing.setOnClickListener {
            switchList(true)
        }

        btTicketHistory = view?.findViewById<Button>(R.id.bt_ticket_history)!!
        btTicketHistory.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_TICKET_LIST_HISTORY), lan)
        btTicketHistory.setOnClickListener {
            switchList(false)
        }

        ticketLayout = view?.findViewById<LinearLayout>(R.id.ticket_layout)!!
        rvContacts = view?.findViewById<View>(R.id.list_ticket) as RecyclerView

        getTicketByUser()




        return view
    }

    fun onItemCopyClickListener(item: JSONObject) {
        Log.i(TAG, "Select item : $item")

        copyTextToClipboard(item.getJSONObject("shop").getString("event_link"))
    }

    fun onItemLinkClickListener(item: JSONObject) {
        Log.i(TAG, "Select item : $item")

        val eventLink = item.getJSONObject("shop").getString("event_link")

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(eventLink))
        startActivity(intent)
    }

    private fun copyTextToClipboard(text : String) {
        val clipboardManager = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clipData = ClipData.newPlainText("text", text)
        clipboardManager.setPrimaryClip(clipData)
        val duration = Toast.LENGTH_SHORT
        val toast = Toast.makeText(requireContext(), "Text copied to clipboard" , duration)
        toast.show()
    }

    private fun getTicketByUser(){

        Log.i(TAG, "getTicketByUser")

        val uid : String = userModel.uid

        val data = JSONObject()
        data.put("uid" , uid)

        firebaseFunctionService.getTicketByUser(data)
            .addOnCompleteListener(OnCompleteListener { task ->


                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details

                        Log.e(TAG, "$code : $details")
                    }
                }else{
                    val result = task.result


                    if (result?.get("status") == "Complete") {

                        val resultData = result["data"]
                        val gson = Gson()
                        val ticketList = JSONArray(gson.toJson(resultData))

                        checkTicketComingOrHistory(ticketList)

                    }else {
                        Log.e(TAG, "Error getShopList")
                    }

                }
            })
            .addOnFailureListener { task ->
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }
    }

    private fun checkTicketComingOrHistory(ticketList: JSONArray){

        this.ticketHistoryList = JSONArray()
        this.ticketComingyList = JSONArray()

        for (i in 0 until ticketList.length()) {

            val ticket = ticketList[i] as JSONObject
            val enddateLong = ticket.getJSONObject("item").getJSONObject("end_date").getLong("_seconds") * 1000
            Log.i(TAG, "enddateLong : $enddateLong")

            val endDate : Date = Date(enddateLong)
            val currentDate : Date = Date()

            Log.i(TAG, "endDate : $endDate , currentDate : $currentDate")

            if(endDate < currentDate){
                ticketHistoryList.put(ticket)
            }else{
                ticketComingyList.put(ticket)
            }
        }

        switchList(true)

    }

    private fun switchList(isComing : Boolean){
        ticketDisplayList = JSONArray()

        Log.i(TAG, "switchList : $isComing")

        btTicketComing.setBackgroundColor(resources.getColor(R.color.gray))
        btTicketHistory.setBackgroundColor(resources.getColor(R.color.gray))

        if(isComing){
            ticketDisplayList = ticketComingyList
            btTicketComing.setBackgroundColor(resources.getColor(R.color.orange))
        }else{
            ticketDisplayList = ticketHistoryList
            btTicketHistory.setBackgroundColor(resources.getColor(R.color.orange))
        }



        val adapter = EventTicketItemAdapter(ticketDisplayList , this)
        rvContacts.adapter = adapter
        rvContacts.layoutManager = LinearLayoutManager(requireContext())


    }
}