package com.ariaround.ariaroundapp.service

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.PhoneAuthProvider
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.time.LocalDate

class QRObject : ViewModel() {

    var qrType : String = ""

    var qrString : String = ""
    var qrData : JSONObject = JSONObject()

    var destinationItem : JSONObject = JSONObject()
    var destinationItemAmount : JSONObject = JSONObject()

}