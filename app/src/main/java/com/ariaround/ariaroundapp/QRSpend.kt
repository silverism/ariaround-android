package com.ariaround.ariaroundapp

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.QRObject
import com.ariaround.ariaroundapp.service.UserObject
import com.budiyev.android.codescanner.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.json.JSONObject
import java.util.HashMap

class QRSpend : Fragment() {

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val qrModel : QRObject by activityViewModels()

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    // layout component
    private lateinit var txTitle : TextView
    private lateinit var codeScanner : CodeScanner

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView

    private lateinit var errorFrame : ConstraintLayout

    // Service
    private lateinit var functions: FirebaseFunctions

    companion object {

        private const val TAG = "QRSpend"
        private var lan = "en"

        private const val REQUEST_CAMERA = 1

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        codeScanner.startPreview()
    }

    override fun onPause() {
        codeScanner.releaseResources()
        super.onPause()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.qr_spend, container, false)


        // Generate Loading view
        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        txTitle = view?.findViewById<TextView>(R.id.tx_title)!!
        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.QR_SCAN_FOR_SPEND), lan)
        txTitle.text = title

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        val scannerView = view?.findViewById<CodeScannerView>(R.id.scanner_view)

        codeScanner = CodeScanner(requireContext(), scannerView)

        // Parameters (default values)
        codeScanner.camera = CodeScanner.CAMERA_BACK // or CAMERA_FRONT or specific camera id
        codeScanner.formats = CodeScanner.ALL_FORMATS // list of type BarcodeFormat,
        // ex. listOf(BarcodeFormat.QR_CODE)
        codeScanner.autoFocusMode = AutoFocusMode.SAFE // or CONTINUOUS
        codeScanner.scanMode = ScanMode.SINGLE // or CONTINUOUS or PREVIEW
        codeScanner.isAutoFocusEnabled = true // Whether to enable auto focus or not
        codeScanner.isFlashEnabled = false // Whether to enable flash or not

        // Callbacks
        codeScanner.decodeCallback = DecodeCallback {
            val result = it.text
            Log.i(TAG , "Scan code : $result")
            scanToSpend(result)
        }
        codeScanner.errorCallback = ErrorCallback { // or ErrorCallback.SUPPRESS
            val errorMessage = it.message
            Log.e(TAG , "Scan code Error : $errorMessage")
            startScan()
        }

        requestCameraPermissions()

        startScan()

        scannerView.setOnClickListener {
            startScan()
        }

        qrModel.qrType = "spend"

        errorFrame = view?.findViewById(R.id.error_frame)

        return view
    }


    private fun startScan(){

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        codeScanner.startPreview()
    }


    private fun requestCameraPermissions() {
        ActivityCompat.requestPermissions(requireActivity() , arrayOf(Manifest.permission.CAMERA), REQUEST_CAMERA)
    }

    private fun scanToSpend(qrCode : String){

        qrModel.qrString = qrCode

        functions = Firebase.functions

        Log.i(TAG, "decryptQR")

//        loadingFrame.visibility = View.VISIBLE

        var req = JSONObject()
        req.put("qrString",qrCode)

        firebaseFunctionService.scanQRForSpend(req)
            .addOnCompleteListener(OnCompleteListener { task ->

//                loadingFrame.visibility = View.INVISIBLE

            if (!task.isSuccessful) {
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG, "$code : $details")
                }
            } else {
                val result = task.result

                if (result?.get("status") == "Complete") {

                    val resultData = result["data"] as HashMap<String, Any>
                    val gson = Gson()
                    val data = JSONObject(gson.toJson(resultData))
                    qrModel.qrData = data
                    Log.i(TAG, "QR data : $data")

                    view?.findNavController()!!.navigate(R.id.action_QRSpend_to_QRSpendItem)


                }else {
                    Log.e(TAG, "Error QR")
                    errorFrame.visibility = View.VISIBLE
                    Handler(Looper.getMainLooper()).postDelayed({
                        errorFrame.visibility = View.INVISIBLE
                    }, 3000)
                }
            }
        })
            .addOnFailureListener { task ->
//                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }
    }



}