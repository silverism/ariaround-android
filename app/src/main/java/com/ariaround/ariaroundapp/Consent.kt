package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService

class Consent : Fragment() {

    private var commonService : CommonService = CommonService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language" , "en") ?: "en"

        var view = inflater.inflate(R.layout.consent, container, false)

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)
        btBack?.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        val txConsentDetail = view?.findViewById<TextView>(R.id.tx_consent_detail)
        txConsentDetail?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_CONFIRM), lan )

        initView(view , lan)

        return view
    }

    fun initView(view : View , lan : String){

        val btConsentAccept : Button = view.findViewById(R.id.bt_next)
        btConsentAccept.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_CONFIRM), lan)
        btConsentAccept.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_consent_to_loginMobile)
        }

        val btConsentTerm : Button = view.findViewById(R.id.bt_consent_term)
        btConsentTerm.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_CONFIRM), lan)
        btConsentTerm.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_consent_to_termCondition)
        }

    }


}