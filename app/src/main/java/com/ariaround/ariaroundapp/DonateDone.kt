package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import org.json.JSONObject

class DonateDone : Fragment() {

    private var commonService : CommonService = CommonService()

    private var lan = "en"

    companion object {
        private const val TAG = "DonateDone"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.donate_done, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        var btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        var btDone = view?.findViewById<Button>(R.id.bt_done)!!
        btDone.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_donateDone_to_QREarnItem)
        }

        var txTitle = view?.findViewById<TextView>(R.id.tx_title)!!
        txTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_DONE_TITLE), lan)

        var txBannerTitle = view?.findViewById<TextView>(R.id.tx_banner_title)!!
        txBannerTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_DONE_BANNER_TITLE), lan)

        var txBannerDetail = view?.findViewById<TextView>(R.id.tx_banner_detail)!!
        txBannerDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_DONE_BANNER_DETAIL), lan)

        return view
    }

    private fun goToQREarnItem(){

    }

}