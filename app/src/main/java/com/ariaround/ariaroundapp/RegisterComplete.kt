package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService

class RegisterComplete : Fragment() {

    private val commonService: CommonService = CommonService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.register_complete, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.CONGRATS_EARN_COIN), lan )
        val txTitle = view?.findViewById<TextView>(R.id.tx_register_complete_earn_coin)
        txTitle?.text = title

        val btNextLabel = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_NEXT) , lan)
        val btNext = view?.findViewById<Button>(R.id.bt_register_complete_next)
        btNext?.text = btNextLabel
        btNext?.setOnClickListener {
            println("Register complete next")
            view?.findNavController().navigate(R.id.action_registerComplete_to_main)
        }

        return view
    }


}