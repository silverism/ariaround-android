package com.ariaround.ariaroundapp

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService


class SettingAbout : Fragment() {

    private val commonService: CommonService = CommonService()

    private var lan = "en"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.setting_about, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        val txTitle = view?.findViewById<TextView>(R.id.tx_title)
        txTitle?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_ABOUT_US), lan )

        val txAboutDetail = view?.findViewById<TextView>(R.id.tx_about_detail)
        txAboutDetail?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ABOUT_DETAIL), lan )

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)
        btBack?.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        val btTerm = view?.findViewById<Button>(R.id.bt_term)
        btTerm?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ABOUT_TERM), lan )
        btTerm?.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_settingAbout_to_termCondition)
        }

        val btSupport = view?.findViewById<Button>(R.id.bt_support)
        btSupport?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ABOUT_SUPPORT), lan )
        btSupport?.setOnClickListener {
            sendMail()
        }

        val btFB = view?.findViewById<ImageButton>(R.id.bt_fb)
        btFB?.setOnClickListener {
            openSocial("https://www.facebook.com/AriAroundTH")
        }

        val btIG = view?.findViewById<ImageButton>(R.id.bt_ig)
        btIG?.setOnClickListener {
            openSocial("https://www.instagram.com/ari.around/")
        }

        return view
    }

    companion object {
        private const val TAG = "SettingAbout"
    }

    private fun sendMail(){
        /* Create the Intent */
        val mIntent = Intent(Intent.ACTION_SEND)
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        mIntent.putExtra(Intent.EXTRA_EMAIL, "hello@ariaround.com")

        startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
    }

    private fun openSocial(urlString : String){
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
        startActivity(intent)
    }
}