package com.ariaround.ariaroundapp

import android.R.attr.phoneNumber
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.UserObject
import com.google.firebase.FirebaseException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import com.google.firebase.auth.PhoneAuthProvider.ForceResendingToken
import com.google.firebase.auth.PhoneAuthProvider.OnVerificationStateChangedCallbacks
import java.util.concurrent.TimeUnit


/**
 * A simple [Fragment] subclass.
 * Use the [Language.newInstance] factory method to
 * create an instance of this fragment.
 */
class LoginMobile : Fragment() {

    private var commonService : CommonService = CommonService()

    private lateinit var mAuth: FirebaseAuth
    private var codeSend : String = ""

    private val userModel: UserObject by activityViewModels()

    private var lan = ""

    private var isMobileComplete = false

    companion object {

        private const val TAG = "LoginMobile"

        private lateinit var btNext : Button
        private lateinit var etPhoneNo : EditText
        private lateinit var navController : NavController

        private lateinit var loadingFrame : FrameLayout
        private lateinit var loadingImage : ImageView
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val view: View = inflater.inflate(R.layout.login_mobile, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language" , "en") ?: "en"

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_PHONE) , lan)
        val txTitleMobile = view.findViewById<TextView>(R.id.tx_title_mobile)
        txTitleMobile?.text = title

        val desc = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_PHONE_DETAIL) , lan)
        val txTitleMobileDesc = view.findViewById<TextView>(R.id.tx_mobile_desc)
        txTitleMobileDesc?.text = desc

        etPhoneNo = view.findViewById<EditText>(R.id.editTextPhone)
        etPhoneNo.addTextChangedListener(textWatcher)



        val btNextLabel = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_NEXT) , lan)
        btNext = view.findViewById<Button>(R.id.bt_next)
        btNext.text = btNextLabel
        btNext.setOnClickListener {
            Log.i(TAG , "Get OTP")
            Log.i(TAG , etPhoneNo.text.toString())
            requestOTP()
        }

        val btBack = view.findViewById<ImageButton>(R.id.bt_back)
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        loadingFrame = view.findViewById<FrameLayout>(R.id.loading_frame)
        loadingFrame.visibility = View.INVISIBLE

        loadingImage = view.findViewById<ImageView>(R.id.loading_image)
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAuth=FirebaseAuth.getInstance()
        navController = view.findNavController()
    }

    private fun requestOTP(){

        if(isMobileComplete) {

            loadingFrame.visibility = View.VISIBLE

            mAuth.setLanguageCode("en")

            val phoneString = etPhoneNo.text.toString().trim()
            Log.i(TAG, phoneString)

            var phoneValue = if (phoneString[0] == '0') {
                "+66" + phoneString.removeRange(0, 1)
            } else {
                "+66$phoneString"
            }

            Log.i(TAG, phoneValue)

            val options = PhoneAuthOptions.newBuilder(mAuth)
                .setPhoneNumber(phoneValue)       // Phone number to verify
                .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
                .setActivity(requireActivity())                 // Activity (for callback binding)
                .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
                .build()
            PhoneAuthProvider.verifyPhoneNumber(options)

        }

    }

    private var mCallbacks: OnVerificationStateChangedCallbacks =
        object : OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                Log.i(TAG , "onVerificationCompleted")
            }
            override fun onVerificationFailed(e: FirebaseException) {
                Log.e(TAG , "${e.message}")
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show();
                loadingFrame.visibility = View.INVISIBLE
            }
            override fun onCodeSent(
                verificationId : String,
                token : ForceResendingToken
            ) {
                super.onCodeSent(verificationId, token)
                loadingFrame.visibility = View.INVISIBLE
                Log.i(TAG , "Code Send : $verificationId")
                userModel.mobileNo = etPhoneNo.text.toString().trim()
                userModel.verificationId = verificationId
                userModel.resendToken = token
                navController.navigate(R.id.action_loginMobile_to_loginOtp)
            }
        }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s != null && s.isNotEmpty()) {
                if (s.toString()[0] == '0' && start == 9){
                    Log.i(TAG ,"Set active button")
                    btNext.setBackgroundColor(resources.getColor(R.color.primary))

                    isMobileComplete = true
                }else if(s.toString()[0] == '8' && start == 8) {
                    btNext.setBackgroundColor(resources.getColor(R.color.primary))
                    isMobileComplete = true
                }else{
                    btNext.setBackgroundColor(resources.getColor(R.color.gray))
                    isMobileComplete = false
                }
            }
        }
    }


}