package com.ariaround.ariaroundapp

import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.QRObject
import com.ariaround.ariaroundapp.service.SpendItemAdapter
import com.ariaround.ariaroundapp.service.UserObject
import org.json.JSONArray
import org.json.JSONObject

class QRSpendItem : Fragment() {

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val qrModel : QRObject by activityViewModels()

    private lateinit var itemList : JSONArray

    // layout component
    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView


    companion object {
        private const val TAG = "QRSpendConfirm"
        private var lan = "en"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.qr_spend_item, container, false)

        // Generate Loading view
        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        val rvContacts = view?.findViewById<View>(R.id.list_item) as RecyclerView


        getItemlist()

        val adapter = SpendItemAdapter(itemList , this)
        rvContacts.adapter = adapter
        rvContacts.layoutManager = LinearLayoutManager(requireContext())

        val txShopName = view?.findViewById<TextView>(R.id.tx_shop_name)
        txShopName.text = qrModel.qrData.getJSONObject("destination").getString("name")

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }


        return view
    }

    private fun getItemlist(){
        itemList = qrModel.qrData.getJSONObject("destination").getJSONArray("item")
    }

    fun onSpendCellClickListener(item: JSONObject) {
        Log.i(TAG , "Select item : $item")
        qrModel.destinationItem = item
        qrModel.qrData.put("value" , (item.getString("value")).toFloat())
        Log.i(TAG , "qrModel : ${qrModel.qrData}")
        view?.findNavController()!!.navigate(R.id.action_QRSpendItem_to_QRConfirm)
    }


}