package com.ariaround.ariaroundapp.service

import android.graphics.Paint
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ariaround.ariaroundapp.EventRegistration
import com.ariaround.ariaroundapp.EventTicketList
import com.ariaround.ariaroundapp.R
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject

class EventTicketItemAdapter(private val mItems: JSONArray, private val cellClickListener: EventTicketList) : RecyclerView.Adapter<EventTicketItemAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val lbEventLink : TextView
        val txEventLink: TextView
        val txEventOpt: TextView
        val imageTicket: ImageView
        val btCopy : ImageButton

        init {
            lbEventLink = itemView.findViewById<TextView>(R.id.lb_event_link)
            txEventLink = itemView.findViewById<TextView>(R.id.tx_event_link)
            txEventLink.paintFlags = Paint.UNDERLINE_TEXT_FLAG
            txEventOpt = itemView.findViewById<TextView>(R.id.tx_event_opt)
            imageTicket = itemView.findViewById<ImageView>(R.id.image_ticket)
            btCopy = itemView.findViewById<ImageButton>(R.id.bt_copy)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.event_ticket_layout, viewGroup, false)
        return ViewHolder(view)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mItems.length()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val ticket: JSONObject = mItems.getJSONObject(position)

        Log.i("EventTicketItemAdapter", "Select ticket : $ticket")

        Picasso.get()
            .load(ticket.getJSONObject("item").getString("ticket_image_url"))
            .placeholder(R.drawable.ic_empty_ticket)
            .into(holder.imageTicket);

        holder.txEventLink.text = ticket.getJSONObject("shop").getString("event_link")
        holder.txEventOpt.text = ticket.getJSONObject("shop").getString("event_opt")

        holder.btCopy.setOnClickListener {
            cellClickListener.onItemCopyClickListener(ticket)
        }

        holder.txEventLink.setOnClickListener {
            cellClickListener.onItemLinkClickListener(ticket)
        }

    }

}
