package com.ariaround.ariaroundapp.service

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import org.json.JSONObject
import kotlin.collections.HashMap

class FirebaseFunctionService {

    private val TAG = "FirebaseFunctionService"
    private val functions: FirebaseFunctions = Firebase.functions

    fun checkExistUser(uid : String) : Task<HashMap<String, Any>> {

        Log.i(TAG, "checkExistUser : $uid")

        return functions
            .getHttpsCallable("checkExistUser")
            .call(uid)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result
            }
    }

    fun getUser(uid : String) : Task<HashMap<String, Any>> {

        Log.i(TAG, "getUser : $uid")

        return functions
            .getHttpsCallable("getUser")
            .call(uid)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>

                result
            }
    }

    fun getShopList(type : String) : Task<HashMap<String, Any>> {

        Log.i(TAG, "getShopList")

        return functions
            .getHttpsCallable("getShopList")
            .call(type)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>

                result
            }
    }

    fun getTicketsByUser(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "getTicketsByUser")

        return functions
            .getHttpsCallable("getTicketsByUser")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>

                result
            }
    }

    fun createUser(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "createUser")

        return functions
            .getHttpsCallable("createUser")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String , Any>
                result
            }

    }

    fun reward(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "reward")

        return functions
            .getHttpsCallable("reward")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String , Any>
                result
            }

    }

    fun checkReferral(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "reward")

        return functions
            .getHttpsCallable("checkReferral")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String , Any>
                result
            }

    }

    fun exchangeCoin(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "exchangeCoin")

        return functions
            .getHttpsCallable("exchangeCoin")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String , Any>
                result
            }
    }

    fun scanQRForSpend(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "scanQRForSpend")

        return functions
            .getHttpsCallable("scanQRForSpend")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }
    }

    fun scanQRForEarn(data : JSONObject) : Task<HashMap<String, Any>> {

        Log.i(TAG, "scanQRForEarn")

        return functions
            .getHttpsCallable("scanQRForEarn")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }
    }

    fun updateProfile(data : JSONObject) : Task<HashMap<String , Any>> {

        Log.i(TAG, "updateUser")

        return functions
            .getHttpsCallable("updateUser")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }

    }

    fun getDonateList() : Task<HashMap<String, Any>> {
        Log.i(TAG, "getDonateList")

        return functions
            .getHttpsCallable("getDonateLocation")
            .call()
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }
    }

    fun redeem(data : JSONObject) : Task<HashMap<String , Any>> {

        Log.i(TAG, "redeem")

        return functions
            .getHttpsCallable("redeem")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }

    }

    fun createTicket(data : JSONObject) : Task<HashMap<String , Any>> {

        Log.i(TAG, "createTicket")

        return functions
            .getHttpsCallable("createTicket")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }

    }

    fun getTicketByUser(data : JSONObject) : Task<HashMap<String , Any>> {

        Log.i(TAG, "getTicketsByUser")

        return functions
            .getHttpsCallable("getTicketsByUser")
            .call(data)
            .continueWith { task ->

                val result = task.result?.data as HashMap<String, Any>
                result

            }

    }

}