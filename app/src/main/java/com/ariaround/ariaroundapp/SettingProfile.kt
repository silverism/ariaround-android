package com.ariaround.ariaroundapp

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.functions.FirebaseFunctionsException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class SettingProfile : Fragment() {

    private val commonService: CommonService = CommonService()

    private var isEditable : Boolean = false

    private val userModel: UserObject by activityViewModels()
    private val TAG = "SettingProfile"
    private var lan = ""

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    private lateinit var spinProfileGender : Spinner
    private lateinit var spinProfileOccupation : Spinner
    private lateinit var btSettingEdit : ImageButton

    private lateinit var lineBirthday : View
    private lateinit var lineFirstname : View
    private lateinit var lineLastname : View
    private lateinit var lineEmail : View
    private lateinit var lineGender : View
    private lateinit var lineOccupation : View

    private lateinit var editFirstname : EditText
    private lateinit var editLastname : EditText
    private lateinit var editEmail : EditText
    private lateinit var btBirthday : Button

    private lateinit var imageSpinGender : ImageView
    private lateinit var imageSpinOccupation : ImageView

    private var selectDateString = ""

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.setting_profile, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        initView(view , lan)

        return view
    }

    private fun initView(view : View , lan : String) {

        isEditable = false

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_TITLE), lan )
        val txTitle = view?.findViewById<TextView>(R.id.tx_setting_profile_title)
        txTitle?.text = title

        val username = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_USERNAME), lan )
        val txUserName = view?.findViewById<TextView>(R.id.tx_setting_profile_username)
        txUserName?.text = username

        val etUsername = view?.findViewById<TextView>(R.id.edittext_setting_profile_username)
        etUsername.text = userModel.userData.getString("username")

        val birthday = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_BIRTHDAY), lan )
        val txBirthday = view?.findViewById<TextView>(R.id.tx_setting_profile_birthday)
        txBirthday?.text = birthday

        val firstname = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_FIRSTNAME), lan )
        val txFirstname = view?.findViewById<TextView>(R.id.tx_setting_profile_firstname)
        txFirstname?.text = firstname

        val txLastname = view?.findViewById<TextView>(R.id.tx_setting_profile_lastname)
        txLastname?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_LASTNAME), lan )

        val txEmail = view?.findViewById<TextView>(R.id.tx_setting_profile_email)
        txEmail?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_EMAIL), lan )

        val txGender = view?.findViewById<TextView>(R.id.tx_setting_profile_gender)
        txGender?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_GENDER), lan )

        val txOccupation = view?.findViewById<TextView>(R.id.tx_setting_profile_occupation)
        txOccupation?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.PROFILE_MENU_OCCUPATION), lan )

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)
        btBack?.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        spinProfileGender = view?.findViewById<Spinner>(R.id.spinner_gender)

        var genderAdapter = if (lan == "en"){
            ArrayAdapter.createFromResource(requireContext(), R.array.SETTING_PROFILE_GENDER_LIST_EN, R.layout.profile_spinner_layout)
        }else{
            ArrayAdapter.createFromResource(requireContext(), R.array.SETTING_PROFILE_GENDER_LIST_TH, R.layout.profile_spinner_layout)
        }
        genderAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinProfileGender.adapter = genderAdapter


        spinProfileOccupation = view?.findViewById<Spinner>(R.id.spinner_occupation)
        var occupationAdapter = if (lan == "en"){
            ArrayAdapter.createFromResource(requireContext(), R.array.SETTING_PROFILE_OCCUPATION_LIST_EN, R.layout.profile_spinner_layout)
        }else{
            ArrayAdapter.createFromResource(requireContext(), R.array.SETTING_PROFILE_OCCUPATION_LIST_TH, R.layout.profile_spinner_layout)
        }
        occupationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinProfileOccupation.adapter = occupationAdapter

        imageSpinGender = view?.findViewById<ImageView>(R.id.image_spinner_gender)
        imageSpinOccupation = view?.findViewById<ImageView>(R.id.image_spinner_occupation)

        btSettingEdit = view?.findViewById<ImageButton>(R.id.bt_setting_profile_edit)
        btSettingEdit.setOnClickListener {
            toggleEditData()
        }

        // Edit view
        editFirstname = view?.findViewById<EditText>(R.id.edittext_setting_profile_firstname)
        editLastname = view?.findViewById<EditText>(R.id.edittext_setting_profile_lastname)
        editEmail = view?.findViewById<EditText>(R.id.edittext_setting_profile_email)

        var cal = Calendar.getInstance()

        val dateSetListener = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)

            val myFormat = "yyyy-MM-dd'T'00:00:00.000'Z'" // mention the format you need 2021-01-01T22:07:00.000Z
            val sdf = SimpleDateFormat(myFormat , Locale("th", "TH"))
            selectDateString = sdf.format(cal.time)

            val btDateFormat = "dd/MM/yyyy"
            val btSdf = SimpleDateFormat(btDateFormat , Locale("th", "TH"))
            btBirthday.text = btSdf.format(cal.time)
        }

        btBirthday = view?.findViewById<Button>(R.id.bt_setting_profile_birthday)
        btBirthday.setOnClickListener {
            DatePickerDialog(requireContext(), dateSetListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()
        }

        if (!userModel.userData.isNull("firstname")){
            editFirstname.setText(userModel.userData.getString("firstname"))
        }

        if (!userModel.userData.isNull("lastname")){
            editLastname.setText(userModel.userData.getString("lastname"))
        }

        if (!userModel.userData.isNull("email")){
            editEmail.setText(userModel.userData.getString("email"))
        }

        if (!userModel.userData.isNull("birthday")){

            val date = Date(userModel.userData.getJSONObject("birthday").getLong("_seconds") * 1000)
            Log.i(TAG , "date : $date")

            val myFormat = "yyyy-MM-dd'T'00:00:00.000'Z'" // mention the format you need 2021-01-01T22:07:00.000Z
            val sdf = SimpleDateFormat(myFormat , Locale("th", "TH"))
            selectDateString = sdf.format(date)

            val btDateFormat = "dd/MM/yyyy"
            val btSdf = SimpleDateFormat(btDateFormat , Locale("th", "TH"))
            btBirthday.text = btSdf.format(date)
        }

        lineBirthday = view?.findViewById<View>(R.id.line_birthday)
        lineFirstname = view?.findViewById<View>(R.id.line_firstname)
        lineLastname = view?.findViewById<View>(R.id.line_lastname)
        lineEmail = view?.findViewById<View>(R.id.line_email)
        lineGender = view?.findViewById<View>(R.id.line_gender)
        lineOccupation = view?.findViewById<View>(R.id.line_occupation)

        stopEdit()

        editFirstname.isEnabled = false
        editLastname.isEnabled = false
        editEmail.isEnabled = false
        btBirthday.isEnabled = false
        spinProfileGender.isEnabled = false
        spinProfileOccupation.isEnabled = false

        // Generate Loading view
        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

    }

    override fun onResume() {
        super.onResume()
        getGenderString()
        getOccupationString()
    }


    private fun getGenderString(){
        val genderValueList = resources.getStringArray(R.array.SETTING_PROFILE_GENDER_LIST_EN).clone()

        for (i in genderValueList.indices) {
            val genderData = genderValueList[i]

            Log.i(TAG , "genderData : $genderData")

            if(!userModel.userData.isNull("gender") && userModel.userData.getString("gender") == genderData){
                Log.i(TAG , "Found genderData : $i")
                spinProfileGender.setSelection(i)
                break;
            }
        }
    }

    private fun getOccupationString(){
        val occupationValueList = resources.getStringArray(R.array.SETTING_PROFILE_OCCUPATION_LIST_EN).clone()

        for (i in occupationValueList.indices) {
            val occupationData = occupationValueList[i]

            Log.i(TAG , "occupationData : $occupationData")

            if(!userModel.userData.isNull("occupation") && userModel.userData.getString("occupation") == occupationData){
                Log.i(TAG , "Found occupationData : $i")
                spinProfileOccupation.setSelection(i)
                break;
            }
        }
    }

    private fun toggleEditData(){
        if (!isEditable){
            startEdit()
            isEditable = true
        }else{

            loadingFrame.visibility = View.VISIBLE

            userModel.userData.put("firstname" , editFirstname.text)
            userModel.userData.put("lastname" , editLastname.text)
            userModel.userData.put("email" , editEmail.text)

            val genderList = resources.getStringArray(R.array.SETTING_PROFILE_GENDER_LIST_EN).clone()
            val gender = genderList[spinProfileGender.selectedItemPosition]

            val occupationList = resources.getStringArray(R.array.SETTING_PROFILE_OCCUPATION_LIST_EN).clone()
            val occupation = occupationList[spinProfileOccupation.selectedItemPosition]

            userModel.userData.put("gender" , gender)
            userModel.userData.put("occupation" , occupation)


            var updateUserData = JSONObject()
            updateUserData.put("uid" , userModel.uid)
            updateUserData.put("firstname" , "${editFirstname.text}")
            updateUserData.put("lastname" , "${editLastname.text}")
            updateUserData.put("email" , "${editEmail.text}")
            updateUserData.put("birthday" , selectDateString)
            updateUserData.put("gender" , gender)
            updateUserData.put("occupation" , occupation)

            Log.i(TAG , updateUserData.toString())

            firebaseFunctionService.updateProfile(updateUserData)
                .addOnCompleteListener(OnCompleteListener { task ->


                    loadingFrame.visibility = View.INVISIBLE

                if(!task.isSuccessful){
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details

                        Log.e(TAG, "$code : $details")

                        if(
                            !userModel.userData.isNull("firstname") && userModel.userData.getString("firstname") != "" && !userModel.userData.isNull("lastname") && userModel.userData.getString("lastname") != "" && !userModel.userData.isNull("gender") && userModel.userData.getString("gender") != "" && !userModel.userData.isNull("occupation") && userModel.userData.getString("occupation") != "" && !userModel.userData.isNull("birthday")
                        ){

                        }
                    }
                }else {
                    val result = task.result

                    if (result?.get("status") == "Complete") {

                        val text = "Complete"
                        val duration = Toast.LENGTH_SHORT

                        val toast = Toast.makeText(requireContext(), text, duration)
                        toast.show()


                    }else {
                        Log.e(TAG, "Error Update")
                    }
                }
            })
                .addOnFailureListener { task ->
                    loadingFrame.visibility = View.INVISIBLE
                    val text = task.toString()
                    val duration = Toast.LENGTH_SHORT
                    val toast = Toast.makeText(requireContext(), text, duration)
                    toast.show()
                    Log.e(TAG, "Firebase service exception : $text")
                }

            stopEdit()
            isEditable = false
        }

        editFirstname.isEnabled = isEditable
        editLastname.isEnabled = isEditable
        editEmail.isEnabled = isEditable
        btBirthday.isEnabled = isEditable
        spinProfileGender.isEnabled = isEditable
        spinProfileOccupation.isEnabled = isEditable
    }

    private fun startEdit(){

        btSettingEdit.setImageResource(R.drawable.ic_done)
        imageSpinGender.visibility = ImageView.VISIBLE
        imageSpinOccupation.visibility = ImageView.VISIBLE
        lineBirthday.visibility = View.VISIBLE
        lineFirstname.visibility = View.VISIBLE
        lineLastname.visibility = View.VISIBLE
        lineEmail.visibility = View.VISIBLE
        lineGender.visibility = View.VISIBLE
        lineOccupation.visibility = View.VISIBLE

    }

    private fun stopEdit(){

        btSettingEdit.setImageResource(R.drawable.ic_edit_profile)
        imageSpinGender.visibility = ImageView.GONE
        imageSpinOccupation.visibility = ImageView.GONE
        lineBirthday.visibility = View.INVISIBLE
        lineFirstname.visibility = View.INVISIBLE
        lineLastname.visibility = View.INVISIBLE
        lineEmail.visibility = View.INVISIBLE
        lineGender.visibility = View.INVISIBLE
        lineOccupation.visibility = View.INVISIBLE

        dismissKeyboard();

    }

    private fun dismissKeyboard() {
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        if (null != requireActivity().currentFocus) imm.hideSoftInputFromWindow(
            requireActivity().currentFocus!!.applicationWindowToken, 0
        )
    }



}