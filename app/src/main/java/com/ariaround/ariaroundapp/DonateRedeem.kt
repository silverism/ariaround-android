package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.QRObject
import com.ariaround.ariaroundapp.service.UserObject
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.gson.Gson
import org.json.JSONArray
import com.google.android.gms.tasks.OnCompleteListener
import org.json.JSONObject

class DonateRedeem : Fragment() {

    private var commonService : CommonService = CommonService()

    private val userModel: UserObject by activityViewModels()
    private val qrModel : QRObject by activityViewModels()
    private var lan = "en"
    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    companion object {
        private const val TAG = "DonateRedeem"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.donate_redeem, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        var btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        var btDone = view?.findViewById<Button>(R.id.bt_done)!!
        btDone.setOnClickListener {
            submitRedeemCode()
        }

        var txTitle = view?.findViewById<TextView>(R.id.tx_title)!!
        txTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_REDEEM_TITLE), lan)

        var txBannerTitle = view?.findViewById<TextView>(R.id.tx_banner_title)!!
        txBannerTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_REDEEM_BANNER_TITLE), lan)

        var txBannerDetail = view?.findViewById<TextView>(R.id.tx_banner_detail)!!
        txBannerDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_REDEEM_BANNER_DETAIL), lan)

        return view
    }

    private fun submitRedeemCode(){
        
        var redeemcode = view?.findViewById<EditText>(R.id.redeemcode)!!

        val uid = userModel.uid

        val redeemdata = JSONObject()
        redeemdata.put("uid" , uid)
        redeemdata.put("shopId" , "YT4Qjdh4GMVKoM2H2ycT")
        redeemdata.put("itemId" , "sA8XwZfzfUY4mHytZLtl")
        redeemdata.put("redeemCode" , redeemcode.text.toString().trim())

        Log.i(TAG , redeemdata.toString())

        firebaseFunctionService.redeem(redeemdata)
            .addOnCompleteListener(OnCompleteListener { task ->

                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details

                        Log.e(TAG, "$code : $details")
                    }
                }else{
                    val result = task.result

                    if (result?.get("status") == "Complete") {

                        qrModel.destinationItem.put("category" , "covid-relief")
                        qrModel.qrData.put("value" , "0")

                        view?.findNavController()?.navigate(R.id.action_donateRedeem_to_QRComplete)

                    }else {
                        Log.e(TAG, "Error redeem")
                    }

                }

            })
            .addOnFailureListener { task ->
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }
    }
}