package com.ariaround.ariaroundapp

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.QRObject
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.json.JSONObject
import java.util.HashMap

class QRConfirm : Fragment() {

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val qrModel : QRObject by activityViewModels()

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    // View component
    private lateinit var txTitle : TextView
    private lateinit var txFromTitle : TextView
    private lateinit var txFrom : TextView
    private lateinit var txFromId : TextView
    private lateinit var txToTitle : TextView
    private lateinit var txTo : TextView
    private lateinit var txToId : TextView
    private lateinit var txItemTitle : TextView
    private lateinit var txItem : TextView
    private lateinit var txValueTitle : TextView
    private lateinit var txValue : TextView
    private lateinit var txValueUnit : TextView

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView

    private lateinit var errorFrame : ConstraintLayout

    companion object {
        private const val TAG = "QRConfirm"
        private var lan = "en"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.qr_confirm, container, false)



        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        val btNext = view?.findViewById<Button>(R.id.bt_next)!!
        btNext.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_NEXT), lan)
        btNext.setOnClickListener {
            submit()
        }

        txTitle = view?.findViewById<TextView>(R.id.tx_title)!!
        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.CONFIRM_PAYMENT), lan)
        txTitle.text = title

        txFromTitle = view?.findViewById<TextView>(R.id.tx_from_title)!!
        val fromTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.CONFIRM_SCAN_FROM), lan)
        txFromTitle.text = fromTitle

        txToTitle = view?.findViewById<TextView>(R.id.tx_to_title)!!
        val toTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.CONFIRM_SCAN_TO), lan)
        txToTitle.text = toTitle

        txItemTitle = view?.findViewById<TextView>(R.id.tx_item_title)!!
        val itemTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.CONFIRM_SCAN_ITEM), lan)
        txItemTitle.text = itemTitle

        txValueTitle = view?.findViewById<TextView>(R.id.tx_value_title)!!
        val valueTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.CONFIRM_SCAN_AMOUNT), lan)
        txValueTitle.text = valueTitle

        txValueUnit = view?.findViewById<TextView>(R.id.tx_value_unit)!!
        val itemUnit = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ARICOIN_UNIT), lan)
        txValueUnit.text = itemUnit

        txFrom = view?.findViewById<TextView>(R.id.tx_from)!!
        txFromId = view?.findViewById<TextView>(R.id.tx_from_id)!!
        txTo = view?.findViewById<TextView>(R.id.tx_to)!!
        txToId = view?.findViewById<TextView>(R.id.tx_to_id)!!
        txItem = view?.findViewById<TextView>(R.id.tx_item)!!
        txValue = view?.findViewById<TextView>(R.id.tx_value)!!

        Log.i(TAG , "qrModel : ${qrModel.qrData.getJSONObject("destination")}")

        if (qrModel.qrType == "spend"){
            txFrom.text = userModel.userData.getString("username")
            txFromId.text = userModel.uid
            txTo.text = qrModel.qrData.getJSONObject("destination").getString("name")
            txToId.text = qrModel.destinationItem.getString("uid")
            txItem.text = qrModel.destinationItem.getString("name_en")
            txValue.text = qrModel.qrData.getString("value")
        }else{
            txFrom.text = qrModel.qrData.getJSONObject("destination").getString("name")
            txFromId.text = qrModel.destinationItem.getString("uid")
            txTo.text = userModel.userData.getString("username")
            txToId.text = userModel.uid
            txItem.text = qrModel.destinationItem.getString("name_en")
            txValue.text = qrModel.qrData.getString("value")
        }


        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        errorFrame = view?.findViewById(R.id.error_frame)


        return view
    }

    private fun submit(){

        loadingFrame.visibility = View.VISIBLE

        Log.i(TAG , "exchangeCoin")

        val reqData = JSONObject()
        reqData.put("qrType" , qrModel.qrType)
        reqData.put("from" , userModel.userData.getString("uid"))
        reqData.put("to" , qrModel.qrData.getJSONObject("destination").getString("uid"))
        reqData.put("value" , qrModel.qrData.getDouble("value"))
        reqData.put("item" , qrModel.destinationItem.getString("uid"))


        firebaseFunctionService.exchangeCoin(reqData)
            .addOnCompleteListener(OnCompleteListener { task ->

                loadingFrame.visibility = View.INVISIBLE

                if(!task.isSuccessful){
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details

                        Log.e(TAG, "$code : $details")
                        errorFrame.visibility = View.VISIBLE
                        Handler(Looper.getMainLooper()).postDelayed({
                            errorFrame.visibility = View.INVISIBLE
                        }, 3000)
                    }
                }else{
                    val result = task.result

                    if (result?.get("status") == "Complete") {

                        if(!qrModel.destinationItem.isNull("ticket_image_url")){

                            loadingFrame.visibility = View.VISIBLE

                            val ticketData = JSONObject()
                            ticketData.put("uid" , userModel.uid)
                            ticketData.put("shopId" , qrModel.qrData.getJSONObject("destination").getString("uid"))
                            ticketData.put("itemId" , qrModel.destinationItem.getString("uid"))

                            firebaseFunctionService.createTicket(ticketData)
                                .addOnCompleteListener(OnCompleteListener { task ->

                                    loadingFrame.visibility = View.INVISIBLE

                                    if(!task.isSuccessful){
                                        val e = task.exception
                                        if (e is FirebaseFunctionsException) {
                                            val code = e.code
                                            val details = e.details

                                            Log.e(TAG, "$code : $details")
                                            errorFrame.visibility = View.VISIBLE
                                            Handler(Looper.getMainLooper()).postDelayed({
                                                errorFrame.visibility = View.INVISIBLE
                                            }, 3000)
                                        }
                                    }else{

                                        val result = task.result

                                        if (result?.get("status") == "Complete") {
                                            val resultData = result["data"] as HashMap<String, Any>
                                            val gson = Gson()
                                            val data = JSONObject(gson.toJson(resultData))
                                            Log.i(TAG, "QR data : $data")

                                            val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
                                            if (sharedPref != null) {
                                                with (sharedPref.edit()) {
                                                    putBoolean("isPopupBanner", true)
                                                    println("sharedPref")
                                                    apply()
                                                }
                                            }

                                            view?.findNavController()!!.navigate(R.id.action_QRConfirm_to_QRComplete)
                                        }else{
                                            Log.e(TAG, "Error QR")
                                            errorFrame.visibility = View.VISIBLE
                                            Handler(Looper.getMainLooper()).postDelayed({
                                                errorFrame.visibility = View.INVISIBLE
                                            }, 3000)
                                        }
                                    }

                                })


                        }else {

                            val resultData = result["data"] as HashMap<String, Any>
                            val gson = Gson()
                            val data = JSONObject(gson.toJson(resultData))
                            Log.i(TAG, "QR data : $data")

                            view?.findNavController()!!.navigate(R.id.action_QRConfirm_to_QRComplete)

                        }
                    }else {
                        Log.e(TAG, "Error QR")
                        errorFrame.visibility = View.VISIBLE
                        Handler(Looper.getMainLooper()).postDelayed({
                            errorFrame.visibility = View.INVISIBLE
                        }, 3000)
                    }
                }
            })
            .addOnFailureListener { task ->
                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }

    }

}