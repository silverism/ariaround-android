package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.UserObject

class RegisterActivity : Fragment() {

    private val commonService: CommonService = CommonService()

    private val activityArray = arrayOf("VOLUNTEER" , "SKILL" , "DONATE" , "OPINION" , "WASTE" , "SHOP")
    private val selectActivity: MutableList<String> = arrayListOf()

    private val userModel: UserObject by activityViewModels()

    companion object {
        private const val TAG = "RegisterActivity"

        private var lan = "en"

        private lateinit var btNext : Button

        private lateinit var btAct1 : Button
        private lateinit var btAct2 : Button
        private lateinit var btAct3 : Button
        private lateinit var btAct4 : Button
        private lateinit var btAct5 : Button
        private lateinit var btAct6 : Button
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.register_activity, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_QUESTION), lan )
        val txTitle = view?.findViewById<TextView>(R.id.tx_register_activity_title)
        txTitle?.text = title

        val act1 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_AWSNER_VOLUNTEER), lan )
        btAct1 = view.findViewById<Button>(R.id.bt_register_activity_1)
        btAct1.text = act1

        val act2 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_AWSNER_SKILL), lan )
        btAct2 = view.findViewById<Button>(R.id.bt_register_activity_2)
        btAct2.text = act2

        val act3 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_AWSNER_DONATE), lan )
        btAct3 = view.findViewById<Button>(R.id.bt_register_activity_3)
        btAct3.text = act3

        val act4 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_AWSNER_OPINION), lan )
        btAct4 = view.findViewById<Button>(R.id.bt_register_activity_4)
        btAct4.text = act4

        val act5 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_AWSNER_WASTE), lan )
        btAct5 = view.findViewById<Button>(R.id.bt_register_activity_5)
        btAct5?.text = act5

        val act6 = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ACTIVITY_AWSNER_SHOP), lan )
        btAct6 = view.findViewById<Button>(R.id.bt_register_activity_6)
        btAct6?.text = act6

        val btBack : ImageButton = view.findViewById<ImageButton>(R.id.bt_back)
        btBack.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        // Set action listener

        btAct1?.setOnClickListener {
            setActivity(0 , btAct1)
        }

        btAct2?.setOnClickListener {
            setActivity(1 , btAct2)
        }

        btAct3?.setOnClickListener {
            setActivity(2 , btAct3)
        }

        btAct4?.setOnClickListener {
            setActivity(3 , btAct4)
        }

        btAct5?.setOnClickListener {
            setActivity(4 , btAct5)
        }

        btAct6?.setOnClickListener {
            setActivity(5 , btAct6)
        }

        btNext = view.findViewById(R.id.bt_next)
        btNext.setOnClickListener {
            if (this.selectActivity.count() == 3){
                userModel.activity = this.selectActivity.toTypedArray()
                view.findNavController().navigate(R.id.action_registerActivity_to_registerThing)
            }
        }

        return view
    }

    private fun setActivity(activity : Int , button : Button){
        val index = this.selectActivity.indexOf(activityArray[activity])
        if(index >= 0){
            this.selectActivity.removeAt(index)
            button.setBackgroundColor(resources.getColor(R.color.light_gray))
        }else{
            this.selectActivity.add(activityArray[activity])
            button.setBackgroundColor(resources.getColor(R.color.primary))
        }

        if (this.selectActivity.count() == 3) {
            btNext.setBackgroundColor(resources.getColor(R.color.primary))
        }else{
            btNext.setBackgroundColor(resources.getColor(R.color.gray))
        }
    }
}