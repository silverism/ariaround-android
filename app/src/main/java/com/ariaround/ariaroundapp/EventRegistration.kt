package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.QRObject
import com.ariaround.ariaroundapp.service.SharedObject
import com.ariaround.ariaroundapp.service.UserObject
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject

class EventRegistration : Fragment() {

    companion object {
        private const val TAG = "EventRegistration"
        private var lan = "en"
    }

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val sharedObj : SharedObject by activityViewModels()
    private var shopData = JSONObject()
    private val qrModel : QRObject by activityViewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.event_registration, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"
        this.shopData = sharedObj.selectShop

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        val txTitle = view?.findViewById<TextView>(R.id.tx_event_registration_title)
        txTitle?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_TITLE), lan )

        val imageCover = view?.findViewById<ImageView>(R.id.event_image_cover)
        Picasso.get()
            .load(shopData.getString("cover_url"))
            .into(imageCover);

        val lbFirstname = view?.findViewById<TextView>(R.id.lb_event_register_firstname)
        lbFirstname?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_FIRSTNAME), lan )

        val lbLastname = view?.findViewById<TextView>(R.id.lb_event_register_lastname)
        lbLastname?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_LASTNAME), lan )

        val lbEmail = view?.findViewById<TextView>(R.id.lb_event_register_email)
        lbEmail?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_EMAIL), lan )

        val lbOccupation = view?.findViewById<TextView>(R.id.lb_event_register_occupation)
        lbOccupation?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_OCCUPATION), lan )

        val txFirstname = view?.findViewById<TextView>(R.id.tx_event_register_firstname)
        txFirstname?.text = if (userModel.userData.isNull("firstname"))  "" else userModel.userData.getString("firstname")

        val txLastname = view?.findViewById<TextView>(R.id.tx_event_register_lastname)
        txLastname?.text = if (userModel.userData.isNull("lastname")) "" else userModel.userData?.getString("lastname")

        val txEmail = view?.findViewById<TextView>(R.id.tx_event_register_email)
        txEmail?.text = if (userModel.userData.isNull("email")) "" else userModel.userData?.getString("email")

        val txOccupation = view?.findViewById<TextView>(R.id.tx_event_register_occupation)
        txOccupation?.text = if (userModel.userData.isNull("occupation")) "" else userModel.userData.getString("occupation")

        val btNext = view?.findViewById<Button>(R.id.bt_next)!!
        btNext.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_CONFIRM), lan)
        btNext.setOnClickListener {
            submit()
        }

        return view;
    }

    private fun submit(){

        val items = shopData.getJSONArray("item") as JSONArray
        val item = items[0] as JSONObject
        val destinationObj = JSONObject()
        destinationObj.put("name" , shopData.getString("name"))
        destinationObj.put("uid" , shopData.getString("uid"))

        qrModel.destinationItem = item
        qrModel.qrType = "spend"
        qrModel.qrData.put("destination" , destinationObj)
        qrModel.qrData.put("value" , (item.getString("value")).toFloat())

        Log.i(TAG, "qrModel : ${qrModel.qrData}")
        view?.findNavController()!!.navigate(R.id.action_eventRegistration_to_QRConfirm)
    }
}