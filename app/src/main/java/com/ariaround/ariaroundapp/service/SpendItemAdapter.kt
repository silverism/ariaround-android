package com.ariaround.ariaroundapp.service

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ariaround.ariaroundapp.QRSpendItem
import com.ariaround.ariaroundapp.R
import org.json.JSONArray
import org.json.JSONObject

class SpendItemAdapter(private val mItems: JSONArray, private val cellClickListener: QRSpendItem) : RecyclerView.Adapter<SpendItemAdapter.ViewHolder>() {

    // Provide a direct reference to each of the views within a data item
    // Used to cache the views within the item layout for fast access
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val txItem: TextView
        val txValue: TextView

        init {
            txItem = itemView.findViewById<TextView>(R.id.tx_spend_item)
            txValue = itemView.findViewById<TextView>(R.id.tx_spend_value)
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        // Create a new view, which defines the UI of the list item
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.qr_spend_item_layout, viewGroup, false)
        return ViewHolder(view)
    }

    // Returns the total count of items in the list
    override fun getItemCount(): Int {
        return mItems.length()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        val item: JSONObject = mItems.getJSONObject(position)

        holder.txItem.text = item.getString("name_en")
        holder.txValue.text = item.getString("value")

        holder.itemView.setOnClickListener {
            cellClickListener.onSpendCellClickListener(item)
        }

    }

}
