package com.ariaround.ariaroundapp

import android.app.AlertDialog
import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.OTPTextWatcher
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import org.json.JSONObject
import java.io.Serializable
import java.util.concurrent.TimeUnit
import kotlin.collections.Map


class LoginOtp : Fragment() {

    private val commonService: CommonService = CommonService()

    private val userModel: UserObject by activityViewModels()

    private lateinit var mAuth: FirebaseAuth
    private lateinit var credential : PhoneAuthCredential

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    private var isCompleteOTP = false
    private var optCode = ""

    private lateinit var txOTP1 : EditText
    private lateinit var txOTP2 : EditText
    private lateinit var txOTP3 : EditText
    private lateinit var txOTP4 : EditText
    private lateinit var txOTP5 : EditText
    private lateinit var txOTP6 : EditText

    private lateinit var btNext : Button

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView

    companion object {

        private const val TAG = "LoginOtp"

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.login_otp, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val title = commonService.getDialogWithLanguage(
            resources.getStringArray(R.array.ENTER_PHONE_VERIFICATION),
            lan
        )
        val txTitleMobile = view?.findViewById<TextView>(R.id.tx_otp_title)
        txTitleMobile?.text = title

        val description = commonService.getDialogWithLanguage(
            resources.getStringArray(R.array.ENTER_PHONE_VERIFICATION_DETAIL),
            lan
        )
        val txOTPDesc = view?.findViewById<TextView>(R.id.tx_otp_desc)
        txOTPDesc?.text = description

        txOTP1 = view.findViewById<EditText>(R.id.otp_edit_box1)!!
        txOTP2 = view.findViewById<EditText>(R.id.otp_edit_box2)!!
        txOTP3 = view.findViewById<EditText>(R.id.otp_edit_box3)!!
        txOTP4 = view.findViewById<EditText>(R.id.otp_edit_box4)!!
        txOTP5 = view.findViewById<EditText>(R.id.otp_edit_box5)!!
        txOTP6 = view.findViewById<EditText>(R.id.otp_edit_box6)!!

        val btNextLabel = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_NEXT) , lan)
        btNext = view.findViewById<Button>(R.id.bt_otp_next)
        btNext?.text = btNextLabel
        btNext?.setOnClickListener {
            verifyOTP(view)
        }

        val txResendOTP = view.findViewById<TextView>(R.id.tx_resend_otp)
        txResendOTP.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_PHONE_VERIFICATION_RESEND) , lan)

        val btResendOTP = view.findViewById<Button>(R.id.bt_resend_otp)
        btResendOTP.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_PHONE_VERIFICATION_RESEND_BUTTON ) , lan)
        btResendOTP.setOnClickListener {
            requestOTP()
        }

        val btBack = view.findViewById<ImageButton>(R.id.bt_back)
        btBack.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }


        setOTPTextField()

        loadingFrame = view.findViewById<FrameLayout>(R.id.loading_frame)
        loadingFrame.visibility = View.INVISIBLE

        loadingImage = view.findViewById<ImageView>(R.id.loading_image)
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAuth=FirebaseAuth.getInstance()
    }



    private fun checkCompleteOTP(){

        this.optCode = txOTP1.text.toString() + "" + txOTP2.text.toString() + "" + txOTP3.text.toString() + "" + txOTP4.text.toString() + "" + txOTP5.text.toString() + "" + txOTP6.text.toString()

        if(optCode.length == 6){
            this.isCompleteOTP = true
            btNext.setBackgroundColor(resources.getColor(R.color.primary))
        }else{
            this.isCompleteOTP = true
            btNext.setBackgroundColor(resources.getColor(R.color.gray))
        }


    }

    private fun requestOTP() {

        loadingFrame.visibility = View.VISIBLE

        mAuth.setLanguageCode("en")

        val phoneString = userModel.mobileNo
        Log.i(TAG, phoneString)

        var phoneValue = if (phoneString[0] == '0') {
            "+66" + phoneString.removeRange(0, 1)
        } else {
            "+66$phoneString"
        }

        Log.i(TAG, phoneValue)

        val options = PhoneAuthOptions.newBuilder(mAuth)
            .setPhoneNumber(phoneValue)       // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(requireActivity())                 // Activity (for callback binding)
            .setCallbacks(mCallbacks)          // OnVerificationStateChangedCallbacks
            .setForceResendingToken(userModel.resendToken)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)

        loadingFrame.visibility = View.INVISIBLE

    }

    private var mCallbacks: PhoneAuthProvider.OnVerificationStateChangedCallbacks =
        object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
                Log.i(TAG , "onVerificationCompleted")
            }
            override fun onVerificationFailed(e: FirebaseException) {
                Log.e(TAG , "${e.message}")
                Toast.makeText(requireContext(), e.message, Toast.LENGTH_LONG).show();
                loadingFrame.visibility = View.INVISIBLE
            }
            override fun onCodeSent(
                verificationId : String,
                token : PhoneAuthProvider.ForceResendingToken
            ) {
                super.onCodeSent(verificationId, token)
                loadingFrame.visibility = View.INVISIBLE
                Log.i(TAG , "Code Send : $verificationId")
                userModel.verificationId = verificationId
                userModel.resendToken = token
            }
        }

    private fun verifyOTP(view : View){

        if (this.isCompleteOTP){
            credential = PhoneAuthProvider.getCredential(userModel.verificationId!!, this.optCode)
            signInWithPhoneAuthCredential(credential , view)
        }
    }

    private fun setOTPTextField() {

        val edit = arrayOf(txOTP1, txOTP2, txOTP3, txOTP4 , txOTP5 , txOTP6)

        txOTP1.addTextChangedListener(OTPTextWatcher(txOTP1, edit))
        txOTP2.addTextChangedListener(OTPTextWatcher(txOTP2, edit))
        txOTP3.addTextChangedListener(OTPTextWatcher(txOTP3, edit))
        txOTP4.addTextChangedListener(OTPTextWatcher(txOTP4, edit))
        txOTP5.addTextChangedListener(OTPTextWatcher(txOTP5, edit))
        txOTP6.addTextChangedListener(OTPTextWatcher(txOTP6, edit))

        txOTP1.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            checkCompleteOTP()
            false
        })

        txOTP2.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                //Perform Code
                txOTP1.text = null
                txOTP1.requestFocus()

                return@OnKeyListener true
            }
            checkCompleteOTP()
            false
        })

        txOTP3.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                //Perform Code
                txOTP2.text = null
                txOTP2.requestFocus()

                return@OnKeyListener true
            }
            checkCompleteOTP()
            false
        })

        txOTP4.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                //Perform Code
                txOTP3.text = null
                txOTP3.requestFocus()

                return@OnKeyListener true
            }
            checkCompleteOTP()
            false
        })

        txOTP5.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                //Perform Code
                txOTP4.text = null
                txOTP4.requestFocus()

                return@OnKeyListener true
            }
            checkCompleteOTP()
            false
        })

        txOTP6.setOnKeyListener(View.OnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DEL) {
                if (txOTP6.text.isNotEmpty()) {
                    txOTP6.text = null
                } else {
                    txOTP5.text = null
                    txOTP5.requestFocus()
                }
                return@OnKeyListener true
            }
            checkCompleteOTP()
            false
        })

    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential , view : View) {
        loadingFrame.visibility = View.VISIBLE
        mAuth.signInWithCredential(credential)
            .addOnCompleteListener(requireActivity()) { task ->
                loadingFrame.visibility = View.INVISIBLE

                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.d(TAG, "signInWithCredential:success")

                    val user = task.result?.user
                    if (user != null) {
                        userModel.uid = user.uid

                        checkExistUser()

                    }


                } else {
                    // Sign in failed, display a message and update the UI
                    Log.w(TAG, "signInWithCredential:failure", task.exception)
                    if (task.exception is FirebaseAuthInvalidCredentialsException) {
                        Log.e(TAG , task.exception.toString())
                        val builder: AlertDialog.Builder? = activity?.let {
                            AlertDialog.Builder(it)
                        }
                        builder?.setMessage("OTP mismatch")?.setTitle("Error")
                        val dialog: AlertDialog? = builder?.create()
                    }
                    // Update UI
                }
            }
    }

    private fun checkExistUser(){

        loadingFrame.visibility = View.VISIBLE

        Log.i(TAG, "checkExistUser")

        firebaseFunctionService.checkExistUser(userModel.uid)
            .addOnCompleteListener(OnCompleteListener { task ->

            loadingFrame.visibility = View.INVISIBLE

            if(!task.isSuccessful){
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG, "$code : $details")
                }
            }
            else{

                val result = task.result

                Log.i(TAG, result?.get("status") as String)

                if (result?.get("status") == "Complete") {
                    val resultData = result["data"] as Map<*, *>
                    val data = JSONObject(resultData)
                    userModel.userData = data
                    view?.findNavController()?.navigate(R.id.action_loginOtp_to_main)

                }else {
                    Log.e(TAG, "Error checkExistUser")

                    view?.findNavController()?.navigate(R.id.action_loginOtp_to_registerName)
                }
            }
        })
            .addOnFailureListener { task ->
                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }

    }
}