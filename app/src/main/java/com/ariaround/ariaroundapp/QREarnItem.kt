package com.ariaround.ariaroundapp

import android.content.Context
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.ariaround.ariaroundapp.service.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject

class QREarnItem : Fragment() {

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val qrModel : QRObject by activityViewModels()

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    private lateinit var itemEarnList : JSONArray
    private lateinit var rvContacts : RecyclerView

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView

    companion object {
        private const val TAG = "QREarnItem"
        private var lan = "en"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view =  inflater.inflate(R.layout.qr_earn_item, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        getShopList()

        rvContacts = view?.findViewById<View>(R.id.list_item) as RecyclerView

        val txTitle = view?.findViewById<TextView>(R.id.tx_title)
        txTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.QR_SCAN_FOR_EARN), lan)

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view?.findNavController().popBackStack(R.id.main , false)
        }

        val btBecomePartner = view?.findViewById<Button>(R.id.bt_become_partner)
        btBecomePartner.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EARN_COIN_BECOME_PARTNER), lan)
        btBecomePartner.setOnClickListener {
            sendMail()
        }

        return view
    }


    fun onEarnCellClickListener(item: JSONObject) {
        qrModel.destinationItem = item

        if (item.getString("category") != "covid-relief") {
            view?.findNavController()!!.navigate(R.id.action_QREarnItem_to_QREarn)
        }else{
            view?.findNavController()!!.navigate(R.id.action_QREarnItem_to_donateRedeem)
        }
    }

//    override fun onItemClick(parent: AdapterView<*>?, view: View, position: Int, id: Long) {
//        Log.i(TAG, "Select Item : $position")
//
//        qrModel.destinationItem = itemEarnList.get(position) as JSONObject
//        view.findNavController().navigate(R.id.action_QREarnItem_to_QREarn)
//    }

    private fun getShopList(){

        Log.i(TAG, "getShopList")

        loadingFrame.visibility = View.VISIBLE

        itemEarnList = JSONArray()

        // Add Covid First

        val covidItem = JSONObject()
        covidItem.put("name_en" , commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_COVID_RELIEF), "en"))
        covidItem.put("name_th" , commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_COVID_RELIEF), "th"))
        covidItem.put("description_en" ,"1 Order = 50 Coin")
        covidItem.put("description_th" ,"1 รายการสั่ง = 50 Coin")
        covidItem.put("category" , "covid-relief")
        covidItem.put("available" , true)

        itemEarnList.put(covidItem)

        firebaseFunctionService.getShopList("earn")
            .addOnCompleteListener(OnCompleteListener { task ->

                loadingFrame.visibility = View.INVISIBLE

            if (!task.isSuccessful) {
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG, "$code : $details")
                }
            }else{
                val result = task.result

                Log.i(TAG, "result : $result")

                if (result?.get("status") == "Complete") {

                    val resultData = result["data"]
                    val gson = Gson()

                    val dataList = JSONArray(gson.toJson(resultData))

                    for (i in 0 until dataList.length()) {

                        val shop = dataList.getJSONObject(i)

                        val itemList = shop.getJSONArray("item")

                        for (j in 0 until itemList.length()) {

                            var item = itemList.getJSONObject(j)

                            item.put("shop_name",shop.getString("name"))
                            item.put("shop_uid",shop.getString("uid"))

                            itemEarnList.put(item)

                        }


                    }

                    Log.i(TAG, "dataList : $itemEarnList")

                    val adapter = EarnItemAdapter(itemEarnList , this)
                    rvContacts.adapter = adapter
                    rvContacts.layoutManager = LinearLayoutManager(requireContext())

                }else {
                    Log.e(TAG, "Error getShopList")
                }

            }
        })
            .addOnFailureListener { task ->
                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }

    }

    private fun sendMail(){
        /* Create the Intent */
        val mIntent = Intent(Intent.ACTION_SEND)
        mIntent.data = Uri.parse("mailto:")
        mIntent.type = "text/plain"
        mIntent.putExtra(Intent.EXTRA_EMAIL, "hello@ariaround.com")

        startActivity(Intent.createChooser(mIntent, "Choose Email Client..."))
    }

}