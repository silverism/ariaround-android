package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService

class SettingLanguage : Fragment() {

    private val commonService: CommonService = CommonService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var view = inflater.inflate(R.layout.setting_language, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        initView(view , lan)

        return view
    }

    private fun initView(view : View , lan : String) {

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_LANGUAGE), lan )
        val txTitle = view?.findViewById<TextView>(R.id.tx_setting_language_title)
        txTitle?.text = title


        val btEN = view?.findViewById<Button>(R.id.bt_setting_language_EN)
        btEN?.setOnClickListener {
            setLanguage("en")
        }

        val btTH = view?.findViewById<Button>(R.id.bt_setting_language_TH)
        btTH?.setOnClickListener {
            setLanguage("th")
        }

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)
        btBack?.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

    }

    private fun setLanguage(lan : String){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return

        with (sharedPref.edit()) {
            putString("language", lan)
            println("sharedPref")
            apply()
        }

        view?.findNavController()?.popBackStack()
    }

}