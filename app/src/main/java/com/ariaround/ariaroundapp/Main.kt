package com.ariaround.ariaroundapp

import android.content.Context
import android.content.Intent
import android.graphics.drawable.AnimationDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import com.google.gson.Gson
import org.json.JSONObject
import java.lang.Float.parseFloat
import java.lang.Long.parseLong
import java.text.SimpleDateFormat
import java.util.*


class Main : Fragment() {

    private val commonService: CommonService = CommonService()

    private val userModel: UserObject by activityViewModels()
    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    companion object {

        private const val TAG = "Main"
        private lateinit var functions: FirebaseFunctions
        private var lan = "en"
        private var isPopupBanner = false

        private lateinit var txUsername : TextView
        private lateinit var txCoin : TextView
        private lateinit var txHistoryDate_1 : TextView
        private lateinit var txHistoryShop_1 : TextView
        private lateinit var txHistoryValue_1 : TextView
        private lateinit var txHistoryDate_2 : TextView
        private lateinit var txHistoryShop_2 : TextView
        private lateinit var txHistoryValue_2 : TextView

        private lateinit var loadingFrame : FrameLayout
        private lateinit var loadingImage : ImageView

        private lateinit var txNotiFullProfile : TextView
        private lateinit var notiFullProfileLayout : ConstraintLayout

        private lateinit var btAritime : ImageButton

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.main, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"
        isPopupBanner = sharedPref?.getBoolean("isPopupBanner", false)!!

        notiFullProfileLayout = view?.findViewById(R.id.noti_full_profile_layout)!!
        notiFullProfileLayout.visibility = View.GONE

        txNotiFullProfile = view?.findViewById(R.id.tx_noti_full_profile)!!
        txNotiFullProfile.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAIN_NOTI_REGISTRATION), lan )

        val btGoToProfile = view?.findViewById<ImageButton>(R.id.bt_go_to_profile)!!
        btGoToProfile?.setOnClickListener {
            view.findNavController().navigate(R.id.action_main_to_settingProfile)
        }

        val earn = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EARN_BUTTON_EARN_NOW), lan )
        val btEarn = view?.findViewById<Button>(R.id.bt_main_earn_coin)
        btEarn?.text = earn

        val history = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAIN_TRANSACTION_HISTORY), lan )
        val txHistory = view?.findViewById<TextView>(R.id.tx_main_history)
        txHistory?.text = history

        btAritime = view?.findViewById<ImageButton>(R.id.bt_main_aritime)
        btAritime?.setOnClickListener {
            gotoAriTime()
        }

        val moreAri = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAIN_BUTTON_MORE_ABOUT_ARIAROUND), lan )
        val btMoreAri = view?.findViewById<Button>(R.id.bt_main_more_about)
        btMoreAri?.text = moreAri
        btMoreAri?.setOnClickListener {
            gotoMoreAri()
        }

        val btRefresh = view?.findViewById<ImageButton>(R.id.bt_refresh)
        btRefresh?.setOnClickListener {
            getUser()
        }

        val scanToSpend = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAIN_SCAN_TO_SPEND), lan )
        val txScanToSpend = view?.findViewById<TextView>(R.id.tx_main_spend_text)
        txScanToSpend?.text = scanToSpend

        val btSetting = view?.findViewById<ImageButton>(R.id.bt_main_setting)
        btSetting?.setOnClickListener {
            view?.findNavController().navigate(R.id.action_main_to_setting)
        }

        val btMap = view?.findViewById<ImageButton>(R.id.bt_main_map)
        btMap?.setOnClickListener {
            view?.findNavController().navigate(R.id.action_main_to_map)
        }

        val btScanToSpend = view?.findViewById<ImageButton>(R.id.bt_main_spend)
        btScanToSpend?.setOnClickListener {
            view?.findNavController().navigate(R.id.action_main_to_QRSpend)
        }

        val btScanToEarn = view?.findViewById<Button>(R.id.bt_main_earn_coin)
        btScanToEarn?.setOnClickListener {
            view?.findNavController().navigate(R.id.action_main_to_QREarnItem)
        }

        txUsername = view.findViewById<TextView>(R.id.tx_main_user)
        txCoin  = view.findViewById<TextView>(R.id.tx_main_coin)
        txHistoryDate_1 = view.findViewById<TextView>(R.id.tx_main_his_1_time)
        txHistoryShop_1 = view.findViewById<TextView>(R.id.tx_main_his_1_name)
        txHistoryValue_1 = view.findViewById<TextView>(R.id.tx_main_his_1_coin)
        txHistoryDate_2 = view.findViewById<TextView>(R.id.tx_main_his_2_time)
        txHistoryShop_2 = view.findViewById<TextView>(R.id.tx_main_his_2_name)
        txHistoryValue_2 = view.findViewById<TextView>(R.id.tx_main_his_2_coin)

        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()



        // For covid banner
        var bannerFrame = view?.findViewById<ConstraintLayout>(R.id.banner_frame)

        if(isPopupBanner) {

            bannerFrame.visibility = FrameLayout.VISIBLE

            var btBannerClose = view?.findViewById<ImageButton>(R.id.bt_banner_close)
            btBannerClose.setOnClickListener {
                val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
                if (sharedPref != null) {
                    with(sharedPref.edit()) {
                        putBoolean("isPopupBanner", false)
                        println("sharedPref")
                        apply()
                    }
                }
                bannerFrame.visibility = FrameLayout.GONE
            }

            var txCovidBannerTitle = view?.findViewById<TextView>(R.id.tx_banner_title)!!
            txCovidBannerTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_BANNER_TITLE), lan)

            var txCovidBannerDetail = view?.findViewById<TextView>(R.id.tx_banner_detail)!!
            txCovidBannerDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_BANNER_DESC), lan)

            var txCovidBannerFooter = view?.findViewById<TextView>(R.id.tx_banner_footer)!!
            txCovidBannerFooter.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_BANNER_DESC_FOOTER), lan)

            var btBanner = view?.findViewById<Button>(R.id.bt_banner)!!
            btBanner.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BT_EVENT_BANNER), lan)
            btBanner.setOnClickListener {
                view?.findNavController().navigate(R.id.action_main_to_eventTicketList)
            }

        }

        return view
    }


    override fun onResume() {
        super.onResume()
        getUser()
    }




    private fun getUser(){

        val uid : String = userModel.uid

        loadingFrame.visibility = View.VISIBLE

        Log.i(TAG, "getUser")

        firebaseFunctionService.getUser(uid).addOnCompleteListener(OnCompleteListener { task ->

            loadingFrame.visibility = View.INVISIBLE

            if(!task.isSuccessful){
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG , "$code : $details")
                }
            }
            else {

                val result = task.result

                if (result?.get("status") == "Complete") {

                    val resultData = result["data"] as HashMap<String, Any>
                    val gson = Gson()
                    val data = JSONObject(gson.toJson(resultData))
                    userModel.userData = data

                    updateDataToView()
                    checkFullProfile()
                } else {
                    Log.e(TAG, "Error checkExistUser")
                }

            }

        })

    }

    private fun updateDataToView(){

        Log.i(TAG , "updateDataToView")

        txUsername.text = userModel.userData.getString("username")

        txCoin.text = "%.2f".format(parseFloat(userModel.userData.getString("balance")))

        val transactions = userModel.userData.getJSONArray("transaction")

        if (transactions.length() > 0) {
            val tx = transactions.get(0) as JSONObject
            txHistoryDate_1.text = getHistoryTime(tx.getJSONObject("created").getString("_seconds"))
            txHistoryShop_1.text = tx.getString("uid_to_name")


            if(tx.getString("qr_type") == "spend"){
                txHistoryValue_1.setTextColor(resources.getColor(R.color.primary))
                txHistoryValue_1.text = "-${tx.getString("value")}"
            }else{
                txHistoryValue_1.text = tx.getString("value")
            }
        }
        if (transactions.length() > 1) {
            val tx = transactions.get(1) as JSONObject
            txHistoryDate_2.text = getHistoryTime(tx.getJSONObject("created").getString("_seconds"))
            txHistoryShop_2.text = tx.getString("uid_to_name")

            if(tx.getString("qr_type") == "spend"){
                txHistoryValue_2.setTextColor(resources.getColor(R.color.primary))
                txHistoryValue_2.text = "-${tx.getString("value")}"
            }else{
                txHistoryValue_2.text = tx.getString("value")
            }

        }
    }

    private fun checkFullProfile(){
        if(
            !userModel.userData.isNull("firstname") && userModel.userData.getString("firstname") != "" && !userModel.userData.isNull("lastname") && userModel.userData.getString("lastname") != "" && !userModel.userData.isNull("gender") && userModel.userData.getString("gender") != "" && !userModel.userData.isNull("occupation") && userModel.userData.getString("occupation") != "" && !userModel.userData.isNull("birthday")
        ){
            notiFullProfileLayout.visibility = View.INVISIBLE
            btAritime.isClickable = true
        }else{
            notiFullProfileLayout.visibility = View.VISIBLE
            notiFullProfileLayout.bringToFront()
            btAritime.isClickable = false
        }

    }

    private fun getHistoryTime(s : String): String?{
        try {
            val sdf = SimpleDateFormat("MM/dd/yyyy HH:mm")
            val netDate = Date(parseLong(s) * 1000)
            return sdf.format(netDate)
        } catch (e: Exception) {
            return e.toString()
        }
    }

    private fun gotoMoreAri(){
        val uriUrl: Uri = Uri.parse(resources.getString(R.string.ARI_ABOUT_URL))
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }

    private fun gotoAriTime(){
        val uriUrl: Uri = Uri.parse(resources.getString(R.string.ARI_TIME_URL))
        val launchBrowser = Intent(Intent.ACTION_VIEW, uriUrl)
        startActivity(launchBrowser)
    }




}