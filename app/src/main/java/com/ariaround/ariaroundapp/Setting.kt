package com.ariaround.ariaroundapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.UserObject
import com.google.firebase.auth.ktx.auth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.ktx.Firebase


class Setting : Fragment() {

    private val commonService: CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()

    companion object {

        private const val TAG = "Setting"

        private lateinit var loadingFrame : FrameLayout
        private lateinit var loadingImage : ImageView

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.setting, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        val lan = sharedPref?.getString("language", "en") ?: "en"

        val title = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_TITLE), lan )
        val txTitle = view?.findViewById<TextView>(R.id.tx_setting_title)
        txTitle?.text = title

        val btProfile = view?.findViewById<Button>(R.id.bt_setting_profile)
        btProfile?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_PROFILE), lan )
        btProfile?.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_setting_to_settingProfile)
        }

        val btLanguage = view?.findViewById<Button>(R.id.bt_setting_language)
        btLanguage?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_LANGUAGE), lan )
        btLanguage?.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_setting_to_settingLanguage)
        }

        val btReferral = view?.findViewById<TextView>(R.id.bt_setting_referral)
        btReferral?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_REFERRAL), lan )
        btReferral?.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_setting_to_settingReferral)
        }

        val version = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_VERSION), lan )
        val txVersion = view?.findViewById<TextView>(R.id.tx_setting_version)
        txVersion?.text = version

        val txAbout = view?.findViewById<TextView>(R.id.bt_setting_about)
        txAbout?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_ABOUT_US), lan )
        txAbout?.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_setting_to_settingAbout)
        }



        val btSettingBack = view?.findViewById<ImageButton>(R.id.bt_back)
        btSettingBack?.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        val txSettingVersion = view?.findViewById<TextView>(R.id.setting_version)
        txSettingVersion?.text = BuildConfig.VERSION_NAME

        val logout = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_SIGNOUT), lan )
        val btLogout = view?.findViewById<TextView>(R.id.bt_setting_logout)
        btLogout?.text = logout

        btLogout?.setOnClickListener {
            logout()
        }

        return view
    }

    private fun logout(){
        userModel.clearData()
        Firebase.auth.signOut()
        view?.findNavController()?.popBackStack(R.id.language , false)
    }
}