package com.ariaround.ariaroundapp

import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.text.LineBreaker
import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.setPadding
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONObject
import android.util.TypedValue




class DonateList : Fragment() {

    private var commonService : CommonService = CommonService()
    private var lan = "en"

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()
    private var donateList = JSONArray()

    companion object {
        private const val TAG = "DonateList"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.donate_list, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        var btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        var txTitle = view?.findViewById<TextView>(R.id.tx_title)!!
        txTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_LIST_TITLE), lan)

        var txHeader = view?.findViewById<TextView>(R.id.tx_header)!!
        txHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_LIST_DETAIL), lan)

        getDonateList()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    private fun getDonateList(){

        donateList = JSONArray()

        firebaseFunctionService.getDonateList()
            .addOnCompleteListener(OnCompleteListener { task ->

                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details

                        Log.e(TAG, "$code : $details")
                    }
                }else{
                    val result = task.result

                    if (result?.get("status") == "Complete") {
                        val resultData = result["data"]
                        val gson = Gson()

                        donateList = JSONArray(gson.toJson(resultData))

                        generateDonateList()

                    }else {
                        Log.e(TAG, "Error getShopList")
                    }

                }

            })
            .addOnFailureListener { task ->
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }

    }

    private fun generateDonateList(){

        val faceLight: Typeface = resources.getFont(R.font.kanit_light);
        val faceBold: Typeface = resources.getFont(R.font.kanit_bold);

        val donateListLayout = view?.findViewById<LinearLayout>(R.id.donate_list_layout)!!

        val newLineLayoutParam = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            1.0f
        )
        newLineLayoutParam.setMargins(0 , 16 , 0 ,0 )

        for (i in 0 until donateList.length()){

            val donate = donateList.getJSONObject(i)

            Log.i(TAG , donate.getString("name"))

            var linearItemLayout = LinearLayout(context)
            linearItemLayout.setBackgroundColor(resources.getColor(R.color.white))
            linearItemLayout.orientation = LinearLayout.VERTICAL

            var itemLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
            )
            itemLayoutParam.setMargins(0 , 32 , 0 ,0 )
            linearItemLayout.layoutParams = itemLayoutParam
            linearItemLayout.setPadding(64)

            // Name and Location button

            var titleLinearLayout = LinearLayout(context)
            titleLinearLayout.orientation = LinearLayout.HORIZONTAL

            val nameParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                0.7f
            )

            var donateName = TextView(context)
            donateName.typeface = faceBold
            donateName.setTextColor(resources.getColor(R.color.black))
            donateName.textSize = 14.0F
            donateName.layoutParams = nameParam
            donateName.setHorizontallyScrolling(false)
            donateName.ellipsize = TextUtils.TruncateAt.END
            donateName.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED

            donateName.text = donate.getString("name")

            val width = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 50F, resources.displayMetrics)

            val btLocationLayoutParam = LinearLayout.LayoutParams(
                width.toInt(),
                width.toInt(),
                0.3f
            )
            var btLocation = ImageButton(context)
            btLocation.setImageResource(R.drawable.ic_location)
            btLocation.layoutParams = btLocationLayoutParam
            btLocation.scaleType = ImageView.ScaleType.FIT_CENTER
            btLocation.setBackgroundColor(resources.getColor(R.color.transparent))
            btLocation.setOnClickListener(View.OnClickListener {
                goToNavigate(donate.getJSONObject("location"))
            })

            titleLinearLayout.addView(donateName)
            titleLinearLayout.addView(btLocation)

            linearItemLayout.addView(titleLinearLayout)

            // Description

            var donateDescription = TextView(context)
            donateDescription.typeface = faceLight
            donateDescription.setTextColor(resources.getColor(R.color.black))
            donateDescription.textSize = 14.0F
            donateDescription.setHorizontallyScrolling(false)
            donateDescription.ellipsize = TextUtils.TruncateAt.END
            donateDescription.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED

            donateDescription.text = donate.getString("description")
            linearItemLayout.addView(donateDescription)

            // Condition

            var requireLinearLayout = LinearLayout(context)
            requireLinearLayout.orientation = LinearLayout.HORIZONTAL
            requireLinearLayout.layoutParams = newLineLayoutParam


            var donateConditionHeader = TextView(context)
            donateConditionHeader.typeface = faceBold
            donateConditionHeader.setTextColor(resources.getColor(R.color.black))
            donateConditionHeader.textSize = 14.0F
            donateConditionHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_LIST_CONDITION), lan)

            var donateCondition = TextView(context)
            donateCondition.typeface = faceLight
            donateCondition.setTextColor(resources.getColor(R.color.black))
            donateCondition.textSize = 14.0F
            donateCondition.setHorizontallyScrolling(false)
            donateCondition.ellipsize = TextUtils.TruncateAt.END
            donateCondition.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED
            donateCondition.text = donate.getString("condition")

            requireLinearLayout.addView(donateConditionHeader)
            requireLinearLayout.addView(donateCondition)

            linearItemLayout.addView(requireLinearLayout)

            // Require Header

            var donateRequireHeader = TextView(context)
            donateRequireHeader.typeface = faceBold
            donateRequireHeader.setTextColor(resources.getColor(R.color.black))
            donateRequireHeader.textSize = 14.0F
            donateRequireHeader.setHorizontallyScrolling(false)
            donateRequireHeader.ellipsize = TextUtils.TruncateAt.END
            donateRequireHeader.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED
            donateRequireHeader.layoutParams = newLineLayoutParam

            donateRequireHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_LIST_REQUIRE), lan)
            linearItemLayout.addView(donateRequireHeader)

            // Require

            var donateRequire = TextView(context)
            donateRequire.typeface = faceLight
            donateRequire.setTextColor(resources.getColor(R.color.black))
            donateRequire.textSize = 14.0F
            donateRequire.setHorizontallyScrolling(false)
            donateRequire.ellipsize = TextUtils.TruncateAt.END
            donateRequire.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED

            val requireArray = donate.getJSONArray("require")
            var requireText = ""
            for (i in 0 until requireArray.length()) {
                requireText += "- " + requireArray.getString(i) + "\n"
            }

            donateRequire.text = requireText
            linearItemLayout.addView(donateRequire)

            // Contact Header

            var donateContactHeader = TextView(context)
            donateContactHeader.typeface = faceBold
            donateContactHeader.setTextColor(resources.getColor(R.color.black))
            donateContactHeader.textSize = 14.0F
            donateContactHeader.setHorizontallyScrolling(false)
            donateContactHeader.ellipsize = TextUtils.TruncateAt.END
            donateContactHeader.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED

            donateContactHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.DONATE_LIST_CONTACT), lan)
            linearItemLayout.addView(donateContactHeader)

            // Contact

            val contactArray = donate.getJSONArray("contact")
            for (i in 0 until contactArray.length()) {

                val contactLinearLayout = LinearLayout(context)
                contactLinearLayout.orientation = LinearLayout.HORIZONTAL

                val contact = contactArray.getJSONObject(i)

                var donateContact = TextView(context)
                donateContact.typeface = faceLight
                donateContact.setTextColor(resources.getColor(R.color.black))
                donateContact.textSize = 14.0F
                donateContact.setHorizontallyScrolling(false)
                donateContact.ellipsize = TextUtils.TruncateAt.END
                donateContact.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED
                donateContact.text = contact.getString("name")

                var donateContactPhone = TextView(context)
                donateContactPhone.typeface = faceLight
                donateContactPhone.setTextColor(resources.getColor(R.color.black))
                donateContactPhone.textSize = 14.0F
                donateContactPhone.setHorizontallyScrolling(false)
                donateContactPhone.ellipsize = TextUtils.TruncateAt.END
                donateContactPhone.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED
                donateContactPhone.text = contact.getString("phone")
                donateContactPhone.paintFlags = Paint.UNDERLINE_TEXT_FLAG
                donateContactPhone.setOnClickListener {
                    makeCall(contact.getString("phone"))
                }

                contactLinearLayout.addView(donateContact)
                contactLinearLayout.addView(donateContactPhone)

                linearItemLayout.addView(contactLinearLayout)

            }

            donateListLayout.addView(linearItemLayout)

        }



    }

    private fun goToNavigate(location : JSONObject){
        val lat = location.getDouble("lat")
        val lon = location.getDouble("lon")

        val strUri = "http://maps.google.com/maps?q=loc:$lat,$lon"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(strUri))
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
        startActivity(intent)
    }

    private fun makeCall(phone : String){
        val phoneNo = phone.replace("-" , "")

        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNo")

        startActivity(intent)
    }


}