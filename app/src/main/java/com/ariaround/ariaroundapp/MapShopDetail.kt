package com.ariaround.ariaroundapp

import android.content.Context
import android.content.Intent
import android.graphics.Paint
import android.graphics.Typeface
import android.graphics.text.LineBreaker
import android.net.Uri
import android.os.Bundle
import android.text.Layout
import android.text.TextUtils
import android.util.Log
import android.view.Gravity
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import androidx.compose.ui.graphics.Color
import androidx.core.view.marginTop
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.SharedObject
import android.view.Display
import android.widget.*
import org.json.JSONObject


class MapShopDetail : Fragment() {

    private var commonService : CommonService = CommonService()


    private var lan = "en"
    private var shopData = JSONObject()

    companion object {
        private const val TAG = "Map"
    }

    private val sharedObj : SharedObject by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        var view = inflater.inflate(R.layout.map_shop_detail, container, false)

        var txTitle = view?.findViewById<TextView>(R.id.tx_title)!!
        txTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_TITLE), lan)

        var txBannerTitle = view?.findViewById<TextView>(R.id.tx_banner_title)!!
        txBannerTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_BANNER_TITLE), lan)

        var txBannerDetail = view?.findViewById<TextView>(R.id.tx_banner_detail)!!
        txBannerDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_BANNER_DETAIL), lan)

        var txOrderHeader = view?.findViewById<TextView>(R.id.tx_order_header)!!
        txOrderHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_ORDER), lan)

        var txOpenTimeHeader = view?.findViewById<TextView>(R.id.tx_opentime_header)!!
        txOpenTimeHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_OPEN_TIME), lan)

        var txOptionHeader = view?.findViewById<TextView>(R.id.tx_option_header)!!
        txOptionHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_OPTION), lan)

        var txMenuHeader = view?.findViewById<TextView>(R.id.tx_menu_header)!!
        txMenuHeader.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_MENU), lan)

        var txFooterTitle = view?.findViewById<TextView>(R.id.tx_footer_title)!!
        txFooterTitle.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_FOOTER_TITLE), lan)

        var txFooterDetail = view?.findViewById<TextView>(R.id.tx_footer_detail)!!
        txFooterDetail.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SHOP_DETAIL_FOOTER_DETAIL), lan)
        txFooterDetail.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        txFooterDetail.setOnClickListener {
            view?.findNavController()?.navigate(R.id.action_mapShopDetail_to_donateList)
        }

        var btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        var btDone = view?.findViewById<Button>(R.id.bt_done)!!
        btDone.setOnClickListener {
            goToDonateDone()
        }



        // Shop Data

        this.shopData = sharedObj.selectShop

        var txShopName = view?.findViewById<TextView>(R.id.tx_shop_name)!!
        txShopName.text = shopData.getString("name")

        var txShopDescription = view?.findViewById<TextView>(R.id.tx_shop_description)!!
        txShopDescription.text = shopData.getString("description_$lan")

        var txOpentime = view?.findViewById<TextView>(R.id.tx_opentime)!!
        txOpentime.text = shopData.getString("opentime_$lan")

        var txOption = view?.findViewById<TextView>(R.id.tx_option)!!
        txOption.text = shopData.getString("option_$lan")

        var btLocation = view?.findViewById<ImageButton>(R.id.bt_location)!!
        if (shopData.getJSONObject("location") != null) {
            btLocation.setOnClickListener {
                goToNavigate()
            }
        }else{
            btLocation.visibility = ImageButton.GONE
        }

        var btFb = view?.findViewById<ImageButton>(R.id.bt_fb)!!
        btFb.setOnClickListener {
            val strUri = shopData.getJSONObject("social_link").getString("fb")
            goToSocial(strUri)
        }

        var btCall = view?.findViewById<ImageButton>(R.id.bt_call)!!
        btCall.setOnClickListener {
            val mobile = shopData.getString("mobile_no")
            makeCall(mobile)
        }

        var btLine = view?.findViewById<ImageButton>(R.id.bt_line)!!
        btLine.setOnClickListener {
            val strUri = shopData.getJSONObject("social_link").getString("line")
            goToSocial(strUri)
        }


        var menuListLayout = view?.findViewById<LinearLayout>(R.id.tx_menu_list)!!

        val faceLight: Typeface = resources.getFont(R.font.kanit_light);
        val faceBold: Typeface = resources.getFont(R.font.kanit_bold);

        for (i in 0 until shopData.getJSONArray("item").length()) {

            var menuItemLayout = LinearLayout(context)
            menuItemLayout.orientation = LinearLayout.HORIZONTAL
            var itemLayoutParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
            )
            itemLayoutParam.setMargins(0 , 32 , 0 ,0 )
            menuItemLayout.layoutParams = itemLayoutParam


            val item = shopData.getJSONArray("item").getJSONObject(i)

            val nameParam = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                0.6f
            )

            var menuItemName = TextView(context)
            menuItemName.typeface = faceLight
            menuItemName.setTextColor(resources.getColor(R.color.black))
            menuItemName.textSize = 14.0F
            menuItemName.layoutParams = nameParam
            menuItemName.setHorizontallyScrolling(false)
            menuItemName.ellipsize = TextUtils.TruncateAt.END
            menuItemName.breakStrategy = LineBreaker.BREAK_STRATEGY_BALANCED

            var menuItemValue = TextView(context)
            menuItemValue.typeface = faceBold
            menuItemValue.setTextColor(resources.getColor(R.color.black))
            menuItemValue.textSize = 14.0F


            menuItemName.text = item.getString("name_$lan")
            menuItemValue.text = item.getDouble("value").toString() + " THB"
            menuItemValue.textAlignment = View.TEXT_ALIGNMENT_VIEW_END
            menuItemValue.layoutParams = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                0.4f
            )

            menuItemLayout.addView(menuItemName)
            menuItemLayout.addView(menuItemValue)

            menuListLayout.addView(menuItemLayout)


        }

        return view
    }

    private fun goToSocial(urlString : String){
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
        startActivity(intent)
    }

    private fun makeCall(phone : String){

        val phoneNo = phone.replace("-" , "")

        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$phoneNo")

        startActivity(intent)
    }

    private fun goToNavigate(){
        val lat = this.shopData.getJSONObject("location").getDouble("lat")
        val lon = this.shopData.getJSONObject("location").getDouble("lon")

        val strUri = "http://maps.google.com/maps?q=loc:$lat,$lon"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(strUri))
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
        startActivity(intent)
    }

    private fun goToDonateDone(){
        view?.findNavController()?.navigate(R.id.action_mapShopDetail_to_donateDone)
    }
}