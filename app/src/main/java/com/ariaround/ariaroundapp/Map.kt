package com.ariaround.ariaroundapp

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.AnimationDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.DrawableRes
import androidx.compose.ui.graphics.Color
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.SharedObject
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.chip.Chip
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.gson.Gson
import com.google.gson.JsonArray
import org.json.JSONArray
import org.json.JSONObject
import java.util.*


class Map : Fragment() , OnMapReadyCallback {

    private var commonService : CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()
    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    private val sharedObj : SharedObject by activityViewModels()

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView

    companion object {

        private const val TAG = "Map"
        private var lan = "en"
        private const val hue_orange = 11F
        private const val hue_pink = 320F
        private const val hue_green = 142F
        private val ari = LatLng(13.7804416, 100.5442001)
        private const val REQUEST_LOCATION = 1

    }
    private lateinit var txMapTitle : TextView
    private lateinit var mapLocationTypeIcon : ImageView
    private lateinit var txMapLocationType : TextView
    private lateinit var txMapLocationCategory : Chip
    private lateinit var txMapLocationName : TextView
    private lateinit var txMapLocationDescription : TextView
    private lateinit var btMapLocationFB : ImageButton
    private lateinit var btMapLocationIG : ImageButton
    private lateinit var btMapLocationIGFilter : ImageButton
    private lateinit var btMapTicketList : ImageButton

    private lateinit var btMapLocationNavigate : Button

    private var locationListSpend : ArrayList<MarkerOptions> = ArrayList()
    private var locationListEarn : ArrayList<MarkerOptions> = ArrayList()
    private var locationListExperience : ArrayList<MarkerOptions> = ArrayList()
    private var locationListCovidRelief : ArrayList<MarkerOptions> = ArrayList()
    private var locationListEvent : ArrayList<MarkerOptions> = ArrayList()

    private lateinit var shopListSpend : ArrayList<JSONObject>
    private lateinit var shopListEarn : ArrayList<JSONObject>
    private lateinit var shopListExperience : ArrayList<JSONObject>
    private lateinit var shopListCovidRelief : ArrayList<JSONObject>
    private lateinit var shopListEvent : ArrayList<JSONObject>

    private lateinit var selectShop : JSONObject
    private lateinit var fusedLocClient: FusedLocationProviderClient


    private lateinit var btMyLocation : Button

    private lateinit var btMapSpend : Button
    private lateinit var btMapEarn : Button
    private lateinit var btMapExperience : Button
    private lateinit var btMapCovidRelief : Button




    private lateinit var functions: FirebaseFunctions
    private lateinit var shopList : JSONArray
    private var locationType = "spend"

    private lateinit var mLocationRequest : LocationRequest
    private lateinit var mMap: GoogleMap
    private lateinit var mapLocation : LinearLayout
    private lateinit var ticketList : JSONArray

    private lateinit var btDonateList : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.map, container, false)

        loadingFrame = view?.findViewById<FrameLayout>(R.id.loading_frame)!!
        loadingFrame.visibility = View.INVISIBLE
        loadingImage = view?.findViewById<ImageView>(R.id.loading_image)!!
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        val mapFragment = childFragmentManager.findFragmentById(R.id.mapview) as SupportMapFragment?
        mapFragment?.getMapAsync(this)
        setupLocClient()
        mLocationRequest = LocationRequest()

        val mapTitle = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_TITLE), lan)
        txMapTitle = view?.findViewById<TextView>(R.id.tx_map_title)!!
        txMapTitle.text = mapTitle

        mapLocation = view?.findViewById<LinearLayout>(R.id.map_location_layout)!!
        mapLocation.visibility = LinearLayout.GONE


        var btBack = view?.findViewById<ImageButton>(R.id.bt_back)!!
        btBack.setOnClickListener {
            view.findNavController().popBackStack()
        }

        btMyLocation = view?.findViewById<Button>(R.id.bt_my_location)!!
        btMyLocation.setOnClickListener {
            Log.i(TAG , "My Location")
            getCurrentLocation()
        }

        btMapSpend = view?.findViewById<Button>(R.id.bt_map_spend)!!
        btMapSpend.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_SPEND), lan)
        btMapSpend.setOnClickListener {
            selectLocationType("spend")
        }

        btMapEarn = view?.findViewById<Button>(R.id.bt_map_earn)!!
        btMapEarn.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_EARN), lan)
        btMapEarn.setOnClickListener {
            selectLocationType("earn")
        }

        btMapExperience = view?.findViewById<Button>(R.id.bt_map_experience)!!
        btMapExperience.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_EXPERIENCE), lan)
        btMapExperience.setOnClickListener {
            selectLocationType("event")
        }

        btMapCovidRelief = view?.findViewById<Button>(R.id.bt_map_covid_relief)!!
        btMapCovidRelief.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_COVID_RELIEF), lan)
        btMapCovidRelief.setOnClickListener {
            selectLocationType("covid-relief")
        }

        mapLocationTypeIcon = view?.findViewById<ImageView>(R.id.map_location_type_icon)!!
        txMapLocationType = view?.findViewById<TextView>(R.id.map_location_type)!!
        txMapLocationCategory = view?.findViewById<Chip>(R.id.map_location_category)!!
        txMapLocationName = view?.findViewById<TextView>(R.id.map_location_name)!!
        txMapLocationDescription = view?.findViewById<TextView>(R.id.map_location_description)!!
        btMapLocationFB = view?.findViewById<ImageButton>(R.id.map_location_fb)!!
        btMapLocationIG = view?.findViewById<ImageButton>(R.id.map_location_ig)!!
        btMapLocationIGFilter = view?.findViewById<ImageButton>(R.id.map_location_ig_filter)!!

        btMapLocationNavigate = view?.findViewById<Button>(R.id.bt_map_location_navigate)!!

        btDonateList = view?.findViewById<TextView>(R.id.bt_donate_list)!!
        btDonateList.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_MAP_TEXT_DONATE_LIST), lan)
        btDonateList.paintFlags = Paint.UNDERLINE_TEXT_FLAG
        btDonateList.setOnClickListener {
            goToNavigate()
        }

        btMapTicketList = view?.findViewById<ImageButton>(R.id.bt_map_ticket_list)
        btMapTicketList.setOnClickListener {
            goToEventTicketList()
        }

        when (this.locationType) {
            "covid-relief" -> {
                btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_MAP_SUPPORT), lan)
                btMapLocationNavigate.setOnClickListener {
                    goToShopDetail()
                }
                btMapLocationNavigate.setBackgroundColor(resources.getColor(R.color.covid_pink))

            }
            "event" -> {
                btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_TITLE), lan)
                btMapLocationNavigate.setOnClickListener {
                    goToEventRegister()
                }
            }
            else -> {
                btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_DIRECTION), lan)
                btMapLocationNavigate.setOnClickListener {
                    goToNavigate()
                }
            }
        }

        btMapLocationFB.setOnClickListener {
            val strUri = selectShop.getJSONObject("social_link").getString("fb")
            goToSocial(strUri)
        }
        btMapLocationIG.setOnClickListener {
            val strUri = selectShop.getJSONObject("social_link").getString("ig")
            goToSocial(strUri)
        }
        btMapLocationIGFilter.setOnClickListener {
            val strUri = selectShop.getJSONObject("social_link").getString("ig_filter")
            goToSocial(strUri)
        }

        btDonateList.setOnClickListener {
            goToShopDetail()
        }



        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap!!
        // Add a marker in Sydney and move the camera

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ari, 15.0F));

        getShopList()
        getEventTicketList()

        googleMap.setOnMarkerClickListener(OnMarkerClickListener { marker ->

            val shopName = marker.title
            val shop : JSONObject = getShopFromName(shopName)

            setLocationData(shop)

            false
        })

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        mMap.isMyLocationEnabled = true

    }

    private fun setLocationData(shop : JSONObject){

        Log.i(TAG , "select shop")
        Log.i(TAG , shop.toString())

        txMapLocationCategory.text = shop.getJSONArray("category")[0].toString()
        txMapLocationName.text = shop.getString("name")
        txMapLocationDescription.text = shop.getString("description_$lan")
        mapLocation.visibility = LinearLayout.VISIBLE


        btMapLocationFB.visibility = Button.GONE
        btMapLocationIG.visibility = Button.GONE
        btMapLocationIGFilter.visibility = Button.GONE

        val mapSocialLayout = view?.findViewById<LinearLayout>(R.id.map_social_layout)!!
        val btDonateList = view?.findViewById<TextView>(R.id.bt_donate_list)!!

        selectShop = shop

        when (this.locationType) {

            "covid-relief" -> { // Covid-relief

                txMapLocationType.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_COVID_RELIEF), lan)

                mapLocationTypeIcon.setImageResource(R.drawable.ic_map_covid_relief);

                mapSocialLayout.visibility = LinearLayout.GONE
                btDonateList.visibility = TextView.VISIBLE

                btDonateList.setOnClickListener {
                    goToDonateList()
                }

                btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.BUTTON_MAP_SUPPORT), lan)
                btMapLocationNavigate.setOnClickListener {
                    goToShopDetail()
                }
                btMapLocationNavigate.setBackgroundColor(resources.getColor(R.color.covid_pink))

            }
            "event" -> {

                val isAlreadyRegisterTicket = checkAlreadyRegisterTicket(shop.getString("uid"))

                Log.i(TAG , "isAlreadyRegisterTicket : $isAlreadyRegisterTicket")

                txMapLocationType.text = locationType

                mapSocialLayout.visibility = LinearLayout.GONE
                btDonateList.visibility = TextView.GONE


                if (isAlreadyRegisterTicket){

                    btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_TICKET_LIST_TITLE), lan)
                    btMapLocationNavigate.setOnClickListener {
                        goToEventTicketList()
                    }

                }else {

                    btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.EVENT_REGISTER_TITLE), lan)
                    btMapLocationNavigate.setOnClickListener {
                        goToEventRegister()
                    }

                    btMapTicketList.visibility = LinearLayout.VISIBLE

                }

                mapLocationTypeIcon.setImageResource(R.drawable.ic_map_experience);
            }
            else -> { //

                txMapLocationType.text = locationType

                mapSocialLayout.visibility = LinearLayout.VISIBLE
                btDonateList.visibility = TextView.GONE

                btMapLocationNavigate.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.MAP_DIRECTION), lan)
                btMapLocationNavigate.setOnClickListener {
                    goToNavigate()
                }

                when (locationType) {
                    "spend" -> {
                        mapLocationTypeIcon.setImageResource(R.drawable.ic_map_spend);
                        btMapLocationFB.visibility = Button.VISIBLE
                        btMapLocationIG.visibility = Button.VISIBLE
                    }
                    "earn" -> {
                        mapLocationTypeIcon.setImageResource(R.drawable.ic_map_earn);
                        btMapLocationFB.visibility = Button.VISIBLE
                    }
                    "experience" -> {
                        mapLocationTypeIcon.setImageResource(R.drawable.ic_map_experience);
                        btMapLocationIGFilter.visibility = Button.VISIBLE
                    }
                }

            }
        }

        when (shop.getJSONArray("category")[0].toString()) {
            "Food & Beverage" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.map_category_food)
            }
            "Lifestyle" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.map_category_lifestyle)
            }
            "Stuff" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.map_category_stuff)
            }
            "AriAround AR" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.map_category_ar)
            }
            "AriAround" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.map_category_ariaround)
            }
            "Local shop" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.covid_pink)
            }
            "Special Event" -> {
                txMapLocationCategory.setChipBackgroundColorResource(R.color.map_category_ar)
            }
        }
    }

    private fun checkAlreadyRegisterTicket(shopId : String): Boolean {

        val uid = userModel.uid
        var isFound = false

        for ( i in 0 until ticketList.length()){
            val ticket : JSONObject = ticketList[i] as JSONObject

            if(ticket.getString("shop_id").equals(shopId)){
                isFound = true
                break
            }

        }

        return isFound


    }

    private fun goToSocial(urlString : String){

        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(urlString))
        startActivity(intent)
    }

    private fun goToNavigate(){
        val lat = selectShop.getJSONObject("location").getDouble("lat")
        val lon = selectShop.getJSONObject("location").getDouble("lon")

        val strUri = "http://maps.google.com/maps?q=loc:$lat,$lon"
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(strUri))
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity")
        startActivity(intent)
    }

    private fun goToEventTicketList(){
        view?.findNavController()?.navigate(R.id.action_map_to_eventTicketList)
    }

    private fun goToDonateList(){
        view?.findNavController()?.navigate(R.id.action_map_to_donateList)
    }

    private fun goToShopDetail(){
        sharedObj.selectShop = this.selectShop
        Log.i(TAG , this.selectShop.toString())
        view?.findNavController()?.navigate(R.id.action_map_to_mapShopDetail)
    }

    private fun goToEventRegister(){
        sharedObj.selectShop = this.selectShop
        Log.i(TAG , this.selectShop.toString())
        view?.findNavController()?.navigate(R.id.action_map_to_eventRegistration)
    }

    private fun getShopList(){

        Log.i(TAG, "getShopList")

        loadingFrame.visibility = View.INVISIBLE

        firebaseFunctionService.getShopList("all")
            .addOnCompleteListener(OnCompleteListener { task ->

                loadingFrame.visibility = View.INVISIBLE

            if (!task.isSuccessful) {
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG, "$code : $details")
                }
            }else{
                val result = task.result


                if (result?.get("status") == "Complete") {

                    val resultData = result["data"]
                    val gson = Gson()
                    val dataList = JSONArray(gson.toJson(resultData))


                    initMapMarker(dataList)

                }else {
                    Log.e(TAG, "Error getShopList")
                }

            }
        })
            .addOnFailureListener { task ->
                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }

    }

    private fun getEventTicketList(){

        Log.i(TAG, "getEventTicketList")

        val uid : String = userModel.uid

        loadingFrame.visibility = View.INVISIBLE

        val data = JSONObject()
        data.put("uid" , uid)

        firebaseFunctionService.getTicketsByUser(data)
            .addOnCompleteListener(OnCompleteListener { task ->


                if (!task.isSuccessful) {
                    val e = task.exception
                    if (e is FirebaseFunctionsException) {
                        val code = e.code
                        val details = e.details

                        Log.e(TAG, "$code : $details")
                    }
                }else{
                    val result = task.result


                    if (result?.get("status") == "Complete") {

                        val resultData = result["data"]
                        val gson = Gson()
                        ticketList = JSONArray(gson.toJson(resultData))

                    }else {
                        Log.e(TAG, "Error getShopList")
                    }

                }
            })
            .addOnFailureListener { task ->
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }
    }

    private fun initMapMarker(dataList : JSONArray){

        shopList = JSONArray()

        shopListSpend = arrayListOf<JSONObject>()
        shopListEarn = arrayListOf<JSONObject>()
        shopListExperience = arrayListOf<JSONObject>()
        shopListCovidRelief = arrayListOf<JSONObject>()
        shopListEvent = arrayListOf<JSONObject>()

        locationListSpend = arrayListOf<MarkerOptions>()
        locationListEarn = arrayListOf<MarkerOptions>()
        locationListExperience = arrayListOf<MarkerOptions>()
        locationListCovidRelief = arrayListOf<MarkerOptions>()
        locationListEvent = arrayListOf<MarkerOptions>()

        try {

            for (i in 0 until dataList.length()) {
                val shop = dataList.get(i) as JSONObject

                shopList.put(shop)

                val lat = shop.getJSONObject("location").getDouble("lat")
                val lon = shop.getJSONObject("location").getDouble("lon")
                val title = shop.getString("name")
                val type = shop.getJSONArray("type")

                val location = LatLng(lat, lon)

                Log.i(TAG, "shop : ${shop.toString()} , type : ${type.toString()}")

                for (j in 0 until type.length()) {

                    when (type.getString(j)) {

                        "spend" -> {
                            val marker = MarkerOptions()
                                .position(location)
                                .title(title)
                                .icon(bitmapDescriptorFromVector(requireContext() , R.drawable.ic_map_spend))

                            locationListSpend.add(marker)
                            shopListSpend.add(shop)
                        }
                        "earn" -> {
                            val marker = MarkerOptions()
                                .position(location)
                                .title(title)
                                .icon(bitmapDescriptorFromVector(requireContext() , R.drawable.ic_map_earn))


                            locationListEarn.add(marker)
                            shopListEarn.add(shop)

                        }
                        "experience" -> {
                            val marker = MarkerOptions()
                                .position(location)
                                .title(title)
                                .icon(bitmapDescriptorFromVector(requireContext() , R.drawable.ic_map_experience))


                            locationListExperience.add(marker)
                            shopListExperience.add(shop)

                        }
                        "covid-relief" -> {
                            val marker = MarkerOptions()
                                .position(location)
                                .title(title)
                                .icon(bitmapDescriptorFromVector(requireContext() , R.drawable.ic_map_covid_relief))


                            locationListCovidRelief.add(marker)
                            shopListCovidRelief.add(shop)
                        }
                        "event" -> {
                            val marker = MarkerOptions()
                                .position(location)
                                .title(title)
                                .icon(bitmapDescriptorFromVector(requireContext() , R.drawable.ic_map_experience))


                            locationListEvent.add(marker)
                            shopListEvent.add(shop)

                        }
                    }
                }

            }

        }catch (error : Exception) {
            Log.e(TAG , "Error create location , ${error.toString()}")
        }

        selectLocationType("covid-relief")
    }

    private fun selectLocationType(type : String){
        locationType = type

        btMapSpend.setBackgroundColor(resources.getColor(R.color.gray))
        btMapEarn.setBackgroundColor(resources.getColor(R.color.gray))
        btMapExperience.setBackgroundColor(resources.getColor(R.color.gray))
        btMapCovidRelief.setBackgroundColor(resources.getColor(R.color.gray))

        mapLocation.visibility = LinearLayout.GONE

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(ari, 15.0F));

        when (locationType) {
            "spend" -> {
                btMapSpend.setBackgroundColor(resources.getColor(R.color.pink))
                addMarker(locationListSpend)
            }
            "earn" -> {
                btMapEarn.setBackgroundColor(resources.getColor(R.color.green))
                addMarker(locationListEarn)
            }
            "experience" -> {
                btMapExperience.setBackgroundColor(resources.getColor(R.color.orange))
                addMarker(locationListExperience)
            }
            "event" -> {
                btMapExperience.setBackgroundColor(resources.getColor(R.color.orange))
                addMarker(locationListEvent)
            }
            "covid-relief" -> {
                btMapCovidRelief.setBackgroundColor(resources.getColor(R.color.covid_pink))
                addMarker(locationListCovidRelief)
            }
        }
    }

    private fun addMarker(locationList : List<MarkerOptions>){

        mMap.clear()

        for (i in locationList.indices) {
            mMap.addMarker(locationList[i])
        }

    }

    private fun getShopFromName(name : String) : JSONObject {

        for (i in 0 until shopList.length()) {
            val shop = shopList.get(i) as JSONObject
            if(shop.getString("name") == name){
                return shop
            }
        }
        return JSONObject()
    }

    private fun getCurrentLocation() {

         if (ActivityCompat.checkSelfPermission(requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) !=
            PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG , "Not permission")
            requestLocPermissions()
        } else {

            Log.i(TAG , "getCurrentLocation")

            mLocationRequest.interval = 0
            mLocationRequest.fastestInterval = 0
            mLocationRequest.numUpdates = 1
            mLocationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
            fusedLocClient.requestLocationUpdates(mLocationRequest, mLocationCallback , Looper.myLooper());

        }
    }

    private var mLocationCallback: LocationCallback? = object : LocationCallback() {

        override fun onLocationResult(locationResult: LocationResult) {

            val locationList = locationResult.locations

            Log.i(TAG , "$locationList")

            if (locationList.size > 0) {
                //The last location in the list is the newest
                val location = locationList[locationList.size - 1]

                //move map camera
                val latLng = LatLng(location.latitude, location.longitude)
                val cameraPosition = CameraPosition.Builder().target(LatLng(latLng.latitude, latLng.longitude)).zoom(16f).build()

                Log.i(TAG , "Move to location")

                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }
        }
    }

    private fun setupLocClient() {
        fusedLocClient = LocationServices.getFusedLocationProviderClient(requireActivity())
    }

    private fun requestLocPermissions() {
//        ActivityCompat.requestPermissions(requireActivity(),
//            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), //permission in the manifest
//            REQUEST_LOCATION)

        Log.i(TAG , "Request permission")

        requestPermissions(
            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
            REQUEST_LOCATION
        )

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        Log.i(TAG , "Permission result")

        if (ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG , "Permission not granted")
            return
        }else {

            Log.i(TAG , "Permission granted")
            mMap.isMyLocationEnabled = true
            getCurrentLocation()
        }
    }

    private fun bitmapDescriptorFromVector(context: Context, vectorResId: Int): BitmapDescriptor? {

        return ContextCompat.getDrawable(context, vectorResId)?.run {
            setBounds(0, 0, intrinsicWidth, intrinsicHeight)
            val bitmap = Bitmap.createBitmap(intrinsicWidth, intrinsicHeight, Bitmap.Config.ARGB_8888)
            draw(Canvas(bitmap))
            BitmapDescriptorFactory.fromBitmap(bitmap)
        }
    }

}