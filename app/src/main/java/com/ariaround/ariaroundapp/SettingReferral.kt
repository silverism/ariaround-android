package com.ariaround.ariaroundapp

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.content.Context.CLIPBOARD_SERVICE
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.CommonService
import com.ariaround.ariaroundapp.service.UserObject


class SettingReferral : Fragment() {

    private val commonService: CommonService = CommonService()
    private val userModel: UserObject by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    companion object {
        private const val TAG = "SettingReferral"
        private var lan = "en"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.setting_referral, container, false)

        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE)
        lan = sharedPref?.getString("language", "en") ?: "en"

        val txTitle = view?.findViewById<TextView>(R.id.tx_title)
        txTitle?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.SETTING_MENU_REFERRAL), lan )

        val txSubTitle = view?.findViewById<TextView>(R.id.tx_subtitle)
        txSubTitle?.text = commonService.getDialogWithLanguage(resources.getStringArray(R.array.ENTER_REFERRAL_DETAIL), lan )

        val txReferralCode = view?.findViewById<TextView>(R.id.tx_referral_code)

        if (!userModel.userData.isNull("referralCode")) {
            txReferralCode?.text = userModel.userData.getString("referralCode")
        }else{
            txReferralCode?.text = ""
        }

        val txQuata = view?.findViewById<TextView>(R.id.tx_quata)
        txQuata?.text = "Quata ${userModel.userData.getString("referralUsed")}/5"

        val btBack = view?.findViewById<ImageButton>(R.id.bt_back)
        btBack?.setOnClickListener {
            view?.findNavController()?.popBackStack()
        }

        val btCopy = view?.findViewById<ImageButton>(R.id.bt_copy)
        btCopy?.setOnClickListener {
            var myClipboard = getSystemService(requireContext(), ClipboardManager::class.java) as ClipboardManager
            val clip: ClipData = ClipData.newPlainText("simple text", txReferralCode?.text)

            myClipboard.setPrimaryClip(clip)

            val text = "Copied"
            val duration = Toast.LENGTH_SHORT

            val toast = Toast.makeText(requireContext(), text, duration)
            toast.show()
        }

        return view
    }


}