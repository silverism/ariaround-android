package com.ariaround.ariaroundapp.service

import androidx.lifecycle.ViewModel
import com.google.firebase.auth.PhoneAuthProvider
import org.json.JSONArray
import org.json.JSONObject
import java.io.Serializable
import java.time.LocalDate

class UserObject : ViewModel() {

    var uid : String = ""
    var mobileNo : String = ""
    var username : String = ""
    var referral : String = ""
    var relation : String = ""
    var activity : Array<String> = arrayOf()
    var thing : Array<String> = arrayOf()
    var verificationId : String = ""
    lateinit var resendToken : PhoneAuthProvider.ForceResendingToken

    var userData : JSONObject = JSONObject()

    fun generateRegisterJSON(): JSONObject {
        var data = JSONObject()

        data.put("uid" , uid)
        data.put("mobileNo" , mobileNo)
        data.put("username" , username)
        data.put("relation" , relation)

        val dataAct = JSONArray()
        for (act in activity) {
            dataAct.put(act)
        }
        data.put("activity" , dataAct)

        val dataThing = JSONArray()
        for (act in thing) {
            dataThing.put(act)
        }
        data.put("thing" , dataThing)

        print(data)

        return data
    }

    fun clearData(){
        uid = ""
        mobileNo = ""
        username = ""
        referral = ""
        relation = ""
        activity = arrayOf()
        thing = arrayOf()
        verificationId = ""

        userData = JSONObject()
    }
}