package com.ariaround.ariaroundapp

import android.content.Context
import android.graphics.drawable.AnimationDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.ariaround.ariaroundapp.service.FirebaseFunctionService
import com.ariaround.ariaroundapp.service.UserObject
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.ktx.auth
import com.google.firebase.functions.FirebaseFunctions
import com.google.firebase.functions.FirebaseFunctionsException
import com.google.firebase.functions.ktx.functions
import com.google.firebase.ktx.Firebase
import org.json.JSONObject
import kotlin.collections.Map


/**
 * A simple [Fragment] subclass.
 * Use the [Language.newInstance] factory method to
 * create an instance of this fragment.
 */
class Language : Fragment() {

    private val userModel: UserObject by activityViewModels()

    private lateinit var loadingFrame : FrameLayout
    private lateinit var loadingImage : ImageView

    private val firebaseFunctionService : FirebaseFunctionService = FirebaseFunctionService()

    companion object {
        private const val TAG = "Language"
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // Inflate the layout for this fragment

        val view: View = inflater.inflate(R.layout.language, container, false)

        val btLanguageEN : Button = view.findViewById(R.id.bt_language_en)
        btLanguageEN.setOnClickListener {
            println("Language Click : EN")
            setLanguage("en")
        }

        val btLanguageTH : Button = view.findViewById(R.id.bt_language_th)
        btLanguageTH.setOnClickListener {
            println("Language Click : TH")
            setLanguage("th")
        }

        loadingFrame = view.findViewById<FrameLayout>(R.id.loading_frame)
        loadingFrame.visibility = View.INVISIBLE

        loadingImage = view.findViewById<ImageView>(R.id.loading_image)
        loadingImage.setBackgroundResource(R.drawable.loading)
        val frameAnimation : AnimationDrawable = loadingImage.background as AnimationDrawable
        frameAnimation.start()

        checkLogin()


        return view
    }



    private fun setLanguage(lan : String){
        val sharedPref = activity?.getPreferences(Context.MODE_PRIVATE) ?: return
        with (sharedPref.edit()) {
            putString("language", lan)
            apply()
        }

        view?.findNavController()?.navigate(R.id.action_language_to_consent)
    }

    private fun checkLogin() {
        val user = Firebase.auth.currentUser
        if (user != null) {
            userModel.uid = user.uid
            checkExistUser()
        }
    }

    private fun checkExistUser(){

        loadingFrame.visibility = View.VISIBLE

        firebaseFunctionService.checkExistUser(userModel.uid)
            .addOnCompleteListener(OnCompleteListener { task ->

            loadingFrame.visibility = View.INVISIBLE

            if(!task.isSuccessful){
                val e = task.exception
                if (e is FirebaseFunctionsException) {
                    val code = e.code
                    val details = e.details

                    Log.e(TAG, "$code : $details")
                }
            }
            else{

                val result = task.result

                Log.i(TAG, result?.get("status") as String)

                if (result?.get("status") == "Complete") {
                    val resultData = result["data"] as Map<String , Any>
                    val data = JSONObject(resultData)
                    userModel.userData = data
                    view?.findNavController()?.navigate(R.id.action_language_to_main)

                }else {
                    Log.e(TAG, "Error checkExistUser")
                }
            }
        })
            .addOnFailureListener { task ->
                loadingFrame.visibility = View.INVISIBLE
                val text = task.toString()
                val duration = Toast.LENGTH_SHORT
                val toast = Toast.makeText(requireContext(), text, duration)
                toast.show()
                Log.e(TAG, "Firebase service exception : $text")
            }
    }



}